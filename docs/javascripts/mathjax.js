window.MathJax = {
	loader: {
		load: ['[tex]/boldsymbol', '[tex]/amsmath', '[tex]/siunitx', '[tex]/color', '[tex]/physics']
	},
	tex: {
		inlineMath: [['$', '$'], ["\\(", "\\)"]],
		displayMath: [['$$', '$$'], ["\\[", "\\]"]],
		
		tags: 'all',
		processEscapes: true,
		numbered: true,
		processEnvironments: true,
		processRefs: true,
		useLabelIds: true,
		processRefs: true,         // process \ref{...} outside of math mode
    	digits: /^(?:[0-9]+(?:\{,\}[0-9]{3})*(?:\.[0-9]*)?|\.[0-9]+)/,

		packages: {'[+]': ['color', 'physics']},
		
		macros:
		{
			// vb: ["\\mathbf{#1}", 1],
			comment: ['\\ \\cdots \\text{(#1)}', 1]
		},
		// packages: {'[+]': ['boldsymbol']},
		tagformat: {
			tag: (n) => '[' + n + ']'
		 },
		baseURL: // URL for use with links to tags (when there is a <base> tag in effect)
			(document.getElementsByTagName('base').length === 0) ?
				'' : String(document.location).replace(/#.*$/, '')),
		formatError:               // function called when TeX syntax errors occur
			(jax, err) => jax.formatError(err)
	},
	options: {
	  ignoreHtmlClass: ".*|",
	  processHtmlClass: "arithmatex"
	}
  };

  document$.subscribe(() => { 
	MathJax.startup.output.clearCache()
	MathJax.typesetClear()
	MathJax.texReset()
	MathJax.typesetPromise()
  })