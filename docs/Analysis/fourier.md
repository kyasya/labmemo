# フーリエ級数展開

波の重ね合わせの話。今は亡き旧サイトの記事を思い出しながらもう一度書いてみた。

# 波の合成

波の主な性質として波の重ね合わせがある。例えば左右から平面波を発生させた場合、それらの波は干渉して山同士、谷同士は重ね合いでより高い、またはより深い谷が、逆に山と谷同士が鑑賞するとお互いの波が打ち消し合うような現象が起こる。
このように干渉した波(合成波)がどのような形になっているのかは波同士の足し合わせによって表すことができる。これは逆に言えば**合成波は複数の単純な波の重ね合わせで表すことができる。**ということになる。

何を言いたいかというと単純な単振動($\sin$、$\cos$)を上手に足し合わせればどんな波でも作れちゃう。ということだ。実際に調べてみよう。

# フーリエ級数

では時間$t$が$-\pi$から$\pi$までの区間で任意の関数$f(t)$が与えられているものとする。更に単振動の合成波である関数$\phi(t)$、

$$
\begin{aligned}
\phi (t) = a_0 &+ a_1 \cos t + a_2 \cos 2t + \cdots + a_n \cos nt \\
&+ b_1 \sin t + b_2 \sin 2t + \cdots + b_n \sin nt \\
= a_0 &+ \sum_{i=1}^{n} (a_i \cos it + b_i \sin it)
\end{aligned} \label{A}
$$

を考える。$a_0, a_1, a_2 \dots a_n$が$\cos$に、$b_0, b_1, b_2 \dots b_n$が$\sin$にかかる任意の定数である。これがある任意の関数$f(t)$に一致する場合を考える。つまり$f(t)$と$\phi(t)$が一致するのだから、任意の点$t=t_1, t_2, \dots, t_{2n+1}$全てに置いて関数の値が一致すること

$$
\left\{
\begin{aligned}
f(t_1) &= \phi(t_1) \\
f(t_2) &= \phi(t_2) \\
 \dots\dots & \dots\dots \\
 \dots\dots & \dots\dots \\
f(t_{2n+1}) &= \phi(t_{2n+1})
\end{aligned}
\right.
$$

が条件であり、この条件が成り立つように定数$a_0, a_1, \dots, a_n, b_1, \dots, b_n$を決定できたと仮定すると、これらの点を増やして更にたくさんの単振動を重ね合わせていったとすると、多くの地点で任意の関数と作った合成波が一致するようになり、**最終的には合成波$\phi(t)$と関数$f(t)$は完全に一致し、合成波は無限級数の形になる**。ただし、無限級数になると上式のように点を連立して解くことは基本不可能なので、逆に無限級数を展開して積分を実行してみる。つまり、

$$
\begin{aligned}
f(t) = \phi (t) = a_0 &+ \sum_{i=1}^{\infty} (a_i \cos it + b_i \sin it)
\end{aligned}
$$

となるので

$$
\begin{aligned}
\int_{-\pi}^{\pi} f(t) dt &= \int_{-\pi}^{\pi} \phi (t) dt \\
= \int_{-\pi}^{\pi} a_0 dt &+ \int_{-\pi}^{\pi} \sum_{i=1}^{\infty} (a_i \cos it + b_i \sin it) dt \\
=  a_0 \int_{-\pi}^{\pi} dt &+ a_1 \int_{-\pi}^{\pi} \cos t dt+ a_2 \int_{-\pi}^{\pi} \cos 2t dt+ \cdots \\
&+ b_1 \int_{-\pi}^{\pi} \sin t dt+ b_2 \int_{-\pi}^{\pi} \sin 2t dt+ \cdots 
\end{aligned} 
$$

となる。ただしここで任意の整数$m$に対して

$$\int_{-\pi}^{\pi} \cos mt dt = 0, \int_{-\pi}^{\pi} \sin mt dt = 0$$

が成り立つので、

$$\int_{-\pi}^{\pi} f(t) dt = 2\pi a_0 $$

これを変形すれば$a_0$は

$$a_0 = \frac{1}{2\pi} \int_{-\pi}^{\pi} f(t) dt$$

である。のこる$a_n$、$b_n$については$\eqref{A}$式に足して両辺に$\cos nt$をかけて積分してやれば、

$$
\begin{aligned}
\int_{-\pi}^{\pi} f(t) \cos nt dt =  a_0 \int_{-\pi}^{\pi} \cos nt dt &+ a_1 \int_{-\pi}^{\pi} \cos nt \cos t dt+ \cdots \\
&+ b_1 \int_{-\pi}^{\pi} \cos nt \sin t dt+ \cdots 
\end{aligned} 
$$

この場合には、

$$
\begin{aligned}
\int_{-\pi}^{\pi} \cos nt dt = 0, &\int_{-\pi}^{\pi} \cos mt \cos nt dt = 0 (m \neq n)\\
\int_{-\pi}^{\pi} \cos^2 nt dt = \pi, &\int_{-\pi}^{\pi} \sin mt \cos nt dt = 0
\end{aligned} 
$$

から、

$$
\begin{aligned}
\int_{-\pi}^{\pi} f(t) \cos nt dt =  a_n \int_{-\pi}^{\pi} \cos^2 nt dt = a_n \pi
\end{aligned} 
$$

と計算できる。故に、

$$a_n = \frac{1}{\pi} \int_{-\pi}^{\pi} f(t) \cos nt dt $$

$b_n$についても同様に$\eqref{A}$式の両辺に$\sin nt$をかけて積分すれば、

$$b_n = \frac{1}{\pi} \int_{-\pi}^{\pi} f(t) \sin nt dt $$

となる。

#### 参考文献

- 有山 正孝 「基礎物理学選書 8: 振動・波動　第38版」(2017).
