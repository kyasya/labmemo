# Analysis

解析に使うプログラミングの知識などのメモ
ソフトはCERN ROOT(C++/Python)、matplotlibあとは解析に向かな過ぎて使いたくないけどExcel etc.

- [Fittingの基礎: 最小二乗法](./least_squares_method.md)
- [ややこしいFittingの「定量的」な評価](./chi2fit.md)
- [定積分の数値計算と誤差(台形法)](./integral.md)
- [フーリエ級数展開](./fourier.md)