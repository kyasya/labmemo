---
description: 物理と競馬と実験の覚書
image: image/banner.png
---

![banner](./image/banner.png#center){width=800}

# 競馬と物理学と実験と

## 何のページ?

趣味でやってる競馬予想(AI)構築と、本業で使う物理学および統計的分析手法(誤差、フィッティング)および解析ツール(c++, Python)の使い方などをメモします。

基礎的なこと、復習したことなどをはじめとしていろいろたらたらと書いていく予定。

### 著者(Author)

きゃしゃ(kyasha)、25歳、学生(大学院生)です。馬獣人です(？)。

![](./image/me.jpg#center){width=300}

人間に擬態した姿はこちら。(？？？)

### 専門分野/趣味

取得学位:修士 (理学) = Master of Science (M.Sc.)

- 本業: 宇宙、素粒子、核実験物理学: 学会などで会えるかも...
- 趣味: 競馬、お絵描き、マイクラ: 競馬オフとかお待ちしております。いや、するのでみんな来てください()
- お酒: ダージリン・クーラーがすき
- 宣伝: 色々プログラムかいてます([下に公開](#作ったやつ))


### 研究会等の予定(近況のみ)

- (完) Jan. 17 - Jan. 19: FPUR研究会 (福井大学, 日本, 参加のみ)
- (完) Jan. 22 - Jan. 24: 検出器とその応用 (KEK, 筑波, 日本)
- (完) Feb. 6 - Feb. 7: [極低放射能研究会 (横浜国立大学, 日本)](https://www.lowbg.org/ugap/ws/lb2023/index.html)
- (完) Mar. 4 - Mar. 5 : [UGAP2024 地下宇宙国際シンポジウム (仙台,日本)](https://www.lowbg.org/ugap/ws/am2023/index.html?LangP=EN&InputContents=Prog)
- (完) Mar. 6 - Mar. 7 : [地下宇宙若手研究会 (仙台, 日本)](https://www.lowbg.org/ugap/ws/yr2023/index.html)
- (完) Jul. 7 : [UGRP領域研究会 (阪大, 大阪, 日本)](https://www.lowbg.org/ugrp/workshop/am2024/index.html)
- (完) Sep. 5 - Sep. 7 : [DMNet 国際シンポジウム (太田, 韓国, **招待講演**)](https://indico.ibs.re.kr/event/664/)
- (完)Sep. 16 - Sep. 19 : [日本物理学会 79回年次大会(札幌, 北大)](https://onsite.gakkai-web.net/jps/jps_search/2024au/index.html)
- (完)Oct. 15 - Oct. 17 : [SMART研究会 2024(伊東, 伊東温泉)](https://smart20170.wixsite.com/smart2024)
- Jun. 27 - Jun 29 : [第39回 研究会「放射線検出器とその応用」(筑波, KEK)](https://conference-indico.kek.jp/event/304/)
- Mar.6 - Mar.7: [第1回 学術変革「地下稀事象」若手研究会(富山)](https://www.lowbg.org/ugrp/workshop/yr2024/)
- Mar.7 - Mar.8: [第10回「極低放射能技術」研究会(富山, 参加のみ)](https://www.lowbg.org/ugrp/workshop/lb2024tmp_0djaGQru1GAO41sRvfqQ/index.html)
- Mar. 18 - Mar. 21: [日本物理学会 2025年春季大会(オンライン)](https://www.jps.or.jp/activities/meetings/spring/spring_index.php)

会う機会があるときはよろしくお願いします。いっぱい議論しましょう！

[これより古い参加情報・実績についてはこちら](./Note/PaperList.md)

### 私用の予定

オフなど私用の予定(大きなやつ)

最近は仕事に忙殺されているため控えめです。

2025はけもけっとに行く予定ですが、学振で死んでるかもしれない。

### 経歴

工業高校の電子科に入学したこともあって電磁気学を通して物理に目覚め、宇宙系のドキュメンタリーを通して宇宙系の物理の研究者になりたいと思うように。
その後進学先を決めるためにオープンキャンパスを回っていたところ宇宙の研究ができるとのことで徳大に進学。行きたいと思ってた研究室に所属して研究してます。

- 1998.12:兵庫県生
- 2017.3: 兵庫県立某工業高等学校 電子科 卒業(パワーエレクトロニクスが専門)
- 2017-2021: 徳島大学 理工学部 応用理数コース 自然科学系 (物理に転向)
- 2021-2023: 徳島大学大学院 創成科学研究科 修士課程
- 2023.4-: 徳島大学大学院 創成科学研究科 博士後期課程
- 2024.9-: 徳島大学 非常勤講師

### 職歴

- ティーチング・アシスタント(TA): 2020 -
- リサーチ・アシスタント(RA): 2023 -
- 康楽賞受賞: 2023
- うずしおスカラー(徳島大学 学際的次世代研究者育成プログラム)採用: 2023 -
- 徳島大学 非常勤講師: 2024.9-

[これより古い参加情報・実績についてはこちら](./Note/PaperList.md)

### 作ったやつ

主にGitにあげてます

[GitHub repository](https://github.com/kyasya/){.md-button}

| 名前とリンク             | 言語             | 説明                                                                                                             |
| ------------------------ | ---------------- | ---------------------------------------------------------------------------------------------------------------- |
| [kplot]                  | ![python]        | 論文用のかっこいい図を書くライブラリを作りたいなと思って。開発途上                                               |
| KBA                      | ![C++]           | K* Basic Analyzer lib.の略。かゆいところに手が届くライブラリ。                                                   |
|                          |                  | B4の後半から開発しているけど、ぐちゃぐちゃなので今最適化中。できたら公開予定                                     |
| [Pandas-like CSV Parser] | ![C++]           | Pandas(in python)と似たことができるライブラリがC++にもあればいいなと思ったので                                   |
|                          |                  | もともとKBAの中にあったCSVパーサでも同じことができたけど、ぐちゃぐちゃなので、                                   |
|                          |                  | 公開のためも考えて一旦分離&再開発中のもの。とりあえず今はパーサとして読み込みだけできる                          |
| PDFConverter             | ![python]        | PDFの切り貼り、写真への変換などをやるためのもの。ネットのフリーサイトは倫理的に                                  |
|                          |                  | 問題があるかと思って開発(@B4)。今はLinuxのBashスクリプトで全部やってるので使ってない                             |
| DAQシステム              | ![C++]&![python] | 我が研究グループのデータ収集システム(DAQ)のハード及びソフトのシステムの全般を担当                                |
|                          |                  | 波形収集、ハードウェア情報の取得&処理、基本的なデータ解析は構築済                                                |
| KWaveformAnalyser        | ![C++]           | ある意味上述のライブラリの一つ。波形情報を解析する。公開可能な部分をまとめて公開するかも                         |
| ez-backup                | ![python]        | バックアップソフト。Bashスクリプトで組もうと思ったけど、クロスプラットフォームに対応すべきだと思ったのでpythonで |
| 便利コマンド             | bash             | 上述したPDFの処理から色々。かゆいところに手が届くやつ                                                            |

[kplot]:https://github.com/kyasya/kplot
[Pandas-like CSV Parser]:https://github.com/kyasya/Pandas-like-csv-parser

[C++]:https://custom-icon-badges.herokuapp.com/badge/C++-f34b7d.svg?logo=Cplusplus&logoColor=white
[python]:https://custom-icon-badges.herokuapp.com/badge/Python-3572A5.svg?logo=Python&logoColor=white
[PowerShell]: https://custom-icon-badges.herokuapp.com/badge/PowerShell-012456.svg?logo=PowerShell&logoColor=white

### Link

 [出版済み論文](./Note/PaperList.md){ .md-button } 出版済み論文など

 [ResearchGate](https://www.researchgate.net/profile/Kenta-Kotera){ .md-button } 私の情報
 
 [我がコースのHP](http://ssip.pm.tokushima-u.ac.jp/~physics/ssp_phys.html){ .md-button } 我がコース(物理科学講座)のHP。(写真がちょっと古いが誰も変えないのである！)

 [我が研究室のHP](https://www-phys.st.tokushima-u.ac.jp/labo/){ .md-button } 研究室のHP。今は見れない。

 [徳島大学HP](https://www.tokushima-u.ac.jp/){ .md-button }
 
マイクラで作っている鉄道と街の詳細など、趣味関係の話は以下のサイトにまとめています。

 [よも市・よも急行電鉄株式会社](https://kyasya.gitlab.io/yomocity/){ .md-button }