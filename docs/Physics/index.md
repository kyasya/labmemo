# Physics

物理学(宇宙・素粒子・放射線)のメモ


<!-- - [](./analytical_mechanics.md) -->
<!-- - [エネルギー分解能](./) -->
<!-- - [エネルギー分解能](./energyresolution.md) -->
- [エネルギー分解能](./energyresolution.md)
- [コンプトン散乱](./compton.md)

## 本講座における参考文献

- 鶴田隆雄 著: 初級放射線 第10版, (2018)
- 八木浩輔 著: 原子核と放射 (朝倉現代物理学講座 - 10), 初版, (1980)
- 久武和夫 & 岡田利弘 著: 原子物理概論, 第三版 (1978)
- 野口正安 著: γ線スペクトロメトリー, 初版 (1980)

一部を除いて古い本だけど、非常に良い本です。絶版が悔やまれます。改訂して再販するべき。
