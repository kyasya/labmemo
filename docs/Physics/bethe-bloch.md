---
description: 荷電粒子と物質の相互作用によるエネルギー損失(bethe-blochの式)と検出器の仕組み
image: image/card/bethe-bloch.png
---

# エネルギー損失: 荷電粒子

![](../image/card/bethe-bloch.png#center)

荷電粒子と物質の相互作用によるエネルギー損失(bethe-blochの式)と検出器の仕組み

## 粒子の「エネルギー」損失と放射線検出器の仕組み

粒子線(α線、β線、中性子をはじめとした他荷電粒子線)は高速で運動する放射線であるので、何かしらの形でエネルギーを失っていき、最終的には速度を完全に失って止まる。止まってしまえばそれらの粒子線の粒子はただの「粒子」である。例えばα線ならヘリウム原子核、β線では電子である。これは粒子の視点から見れば粒子がエネルギーを失ったことになるが、何かしらの物質から見ればエネルギーを**受け取った**といえる。この**エネルギー損失**(energy loss)の現象をうまく利用して、特定の物質から電荷量として取り出す電気装置が**検出器**である。検出器は放射線が物質とあらゆる相互作用をしてエネルギーを失い、その過程で光となったり電子を出したりする。その電子を直接、光であれば電子に変換して増幅することで電流信号として取り出しているのである。

では「荷電粒子」におけるエネルギー損失の現象を古典論的に見ていこう。

## 電子との相互作用によるエネルギー損失

荷電粒子が物質中を通過する時、物質中を構成している原子と衝突を繰り返してエネルギー損失する(俗にエネルギーを落とすと言われる)。この「衝突」は粒子線の粒子と、原子中の電子が相互作用することを意味し、これを特に**電子損失**と呼ぶ。これは静止している原子核の電子に対して、粒子線が飛んでくるとみなせるため、絵にすると以下のようになる。

![](../image/physics/bethe-bloch.png#center)

粒子線が持つ質量は$M$、電荷量は$Ze$である。電子は質量が$m$、電荷量は$e$である。荷電粒子は電子から$b$だけ離れた位置で一定の速さ$v$で運動している。さらにここで、電子と粒子線には長さ$b$の円筒空間を考えることにする。このとき電磁相互作用により電子は$Ze$に応じた衝撃により、運動量、つまりエネルギーを受け取って運動する。このモデルでは$b$方向に垂直な成分だけを考えればよい[^1]。この時電子が受け取る衝撃は力積$I$に酔って記述できて、

[^1]:
    水平成分における電場はある区間($x_1,x_2$)まで平均すると0になるため

$$\begin{align}
    I=\int_{-\infty}^{\infty} F(t) dt=\int_{-\infty}^{\infty} E(t) dt =\int_{-\infty}^{\infty} E(x) \frac{dt}{dx}dx=\frac{e}{v}\int_{-\infty}^{\infty} E(x) dx
\end{align}$$

となる。ここで円筒空間を仮定したのでGaussの法則により、

$$\begin{align}
    \int_{-\infty}^{\infty} E(x) dx = \frac{2Ze}{b}
\end{align}$$

となり、電子の運動量は0であるから力積と運動量の関係から電子に与えられる運動量は、

$$\begin{align}
    \Delta p = I = \dfrac{2Ze^2}{vb}
\end{align}$$

となり、$E=p^2/2m$よりエネルギー$E$に書き換えると

$$\begin{align}
    \Delta E=\dfrac{2Z^2e^4}{mv^2b^2}
\end{align}$$

となる。電子と荷電粒子が**失う**エネルギー損失は電子と荷電粒子の距離$b$に比例するので前までの結果を微分すれば良くて、$db$に対して

$$\begin{align}
    dE=-\Delta E\ N_e 2\pi b\ dbdx = \dfrac{4Z^2e^4}{mv^2} N_e \frac{dx}{b} db
\end{align}$$

これを有限な範囲$b_\mathrm{min}$から$b_\mathrm{max}$で積分してやると

$$\begin{align}
    \int_{b_\mathrm{min}}^{b_\mathrm{max}} dE &=-\dfrac{4\pi Z^2e^4N_e}{mv^2}dx \int_{b_\mathrm{min}}^{b_\mathrm{max}} \frac{1}{b} db \notag \\
    &=-\dfrac{4\pi Z^2e^4N_e}{mv^2}dx \ln \left| \dfrac{b_\mathrm{max}}{b_\mathrm{min}} \right| = dE(x)
\end{align}$$

よって、微小移動距離におけるエネルギー損失の式$dE/dx$、

$$\begin{align}
    \dfrac{dE}{dx} &=-\dfrac{4\pi Z^2e^4N_e}{mv^2} \ln \left| \dfrac{b_\mathrm{max}}{b_\mathrm{min}} \right| \tag{a}\label{eq:dedx}
\end{align}$$

が得られる。この$b$の有限範囲の上限及び下限は相対論と量子論を考慮することで設定できる。ド・ブロイの式$\lambda=h/p$から下限値は電子の運動量を用いて計算できる。ここで$b$が最小となる条件は粒子同士が正面衝突することである。よって電子が受け取るエネルギーは$m(2v)^2/2$とかけるが、ここで粒子は高速で運動していることに注意すれば相対論的効果により、

$$\begin{align}
    \Delta E=\dfrac{2Z^2e^4}{mv^2b_\mathrm{min}^2} = 2 \gamma^2 m v^2 \\ \therefore b_\mathrm{min}=\dfrac{Ze^2}{\gamma m_e v^2}
\end{align}$$

とかける。ここで$\gamma$はローレンツ因子で$\gamma=(1-\beta^2)^{-1/2},\ \beta=v/c$である。上限値については実際の電子が原子によって束縛されていることを思い出せばよい。電子は束縛されているので、電子がエネルギーを吸収するには周期的境界条件から束縛された電子の軌道の周期$\tau$と同程度となる必要があると考えられる。よって、衝突時間を$\Delta t$とすると$\Delta t \leq \tau$となるから$v=b/v$を用いて、

$$\begin{align}
    \Delta t= \frac{b}{\gamma v} \leq \frac{1}{\bar{\nu}}
\end{align}$$

が条件となる。$\bar{\nu}$は電子軌道における束縛周波数である。ただし、相互作用における束縛状態は様々なパターンが考えられるため、平均値を使用した。よって上限は、

$$\begin{align}
    b_\mathrm{max} = \dfrac{\gamma v}{\bar{\nu}}
\end{align}$$

となる。よって$\eqref{eq:dedx}$にこれらの上下限値を代入して、

$$\begin{align}
    \dfrac{dE}{dx} &=-\dfrac{4\pi N_e}{mv^2} Z^2e^4 \ln \left| \dfrac{\gamma^2 m v^3}{Ze^2 \nu} \right| \tag{b}\label{eq:dedx2}
\end{align}$$

となる。この式は重たい粒子(α線程度以上の質量の粒子線)にはよく成り立つが、一方で軽い粒子については合わない。

## bethe-blochの式

前項の軽い粒子に対して$\eqref{eq:dedx}$が合わない理由は軽い粒子における量子効果を考慮してないためである。量子効果を考慮した結果はBethe(ベーテ)とBloch(ブロッホ)によりなされた。ここでは結果だけを示す。

$$\begin{align}
    \dfrac{dE}{dx} &=- \dfrac{4\pi N_e}{mv^2} Z^2e^4 \left[ \ln \frac{2 v^2 m }{I(1-\beta^2)}-\beta^2 \right]
\end{align}$$

ここで$I$は物質の原子のイオン化ポテンシャルの平均値で、$I=10~\mathrm{eV} Z$と近似される。理論と実際の例をいかに示す。([PDGより](https://pdg.lbl.gov/))

![](../image/physics/bethe-bloch2.png#center){width=800}

左が理論、右が実際に見られるエネルギー損失の様子である(軸の単位と大きさ(桁)に注意)。荷電粒子によって、エネルギー損失の挙動が異なることが確認できる。この挙動の仕方はデータ解析における粒子識別に利用される。