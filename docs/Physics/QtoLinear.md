---
description: 電荷量から信号波形の最大振幅を見積もろうという話
image: image/card/QtoLinear.png
---

# 電荷量から信号波形の最大振幅を見積もろうという話

![](../image/card/QtoLinear.png#center)

光電子増倍管の信号波形の振幅を簡単に求める方法について考えた話。本測定前の確認に使えそう。

## PMTのGain

光電子増倍管(PMT)の電圧に対する増幅率(Gain)$A$は

$$\begin{align} A = \dfrac{V_\mathrm{aft.}}{V_\mathrm{bef.}} = \dfrac{Q_\mathrm{aft.}}{Q_\mathrm{bef.}}\end{align}$$

で与えられる。当然であるがPMTのGainはP印加する電圧(HV)の大きさに比例する。よく『電圧を100 V上げるとGainが2倍程度上がる』といわれるが、これをもう少し丁寧に確かめることはできないか？ということを考えたのがすべての始まりである。というのも**測定装置には最大電圧定格があり、それ以上大きな電圧を入れてもきちんとした波形はとってくれない**。
つまりきちんとGainを設定しないと、増幅不足or過剰で見たいエネルギーのデータが取得できないということが起こり得る。

<figure markdown>
![](../image/physics/FADCSaturation.png#center){width=700}
<figcaption>最大定格より大きな電圧信号の波形は正確に記録してくれない</figcaption>
</figure>

最も簡単なのは標準線源を入れたあと信号波形をみて、全吸収ピークの波形の最大値とエネルギーの較正直線を作ることであるが、えいやと決め打ちして直線を引くのは不定性がでかい。大雑把な調整ではそれでいいが、本当に見たいエネルギーまで取得できるかを「確かめる」という意味では不安要素が残る。

そこである程度の精度をもちつつ、エネルギー較正などの片手間にさっと確かめられるようなモデルを構築することを目標とした。[^1]

[^1]:
    より厳密なモデルや測定方法はあるだろう…というか頭にほぼ正解が思い浮かんでいる感じがするが、今回は光電子のゆらぎは考えないことにしよう。実際エネルギー較正程度では量子的なゆらぎはあまり気にしなくても良さそうだ。

## どのように信号の最大振幅を復元すればよいか

単純な話、電圧情報を知りたいのであれば全波形の振幅の最大値を片っ端から解析してやって、電圧の最大値に対するスペクトルを書いてやればよいわけだが、値の不定性がでかく、精度がわるわるである。[^2] 対して電荷は実際に波形を積分して求めるため、不定性が小さく、不定性がでかい信号電圧の最大値のスペクトルをわざわざ作る必要もないというのもある。

[^2]:
    故に分解能が悪く、ピークがあるのはかろうじてわかるが、それを頑張って読み取って調べるのは難しい。実際複数のピークが一つのピークに見えてしまうことがよくある。

## モデル

非常に単純なシンチレーション信号のモデルとして、電圧値$V$がある最大値$V_\mathrm{max}$から指数関数的に減少していく関数を考える。つまりLinear信号は

$$\begin{align} V=V(t)=V_\mathrm{max} e^{-\frac{t}{\tau}}\end{align}$$

という関数で与えられる。ここで$\tau$はシンチレーション信号の減衰時間(時定数)である。

<figure markdown>
![](../image/physics/IdealScintillationWave.png#center){width=500}
<figcaption>波形のモデル</figcaption>
</figure>

ここで微小区間における電荷は$dQ=Idt$であるから、全電荷$Q$は、

$$\begin{align} Q = \int_{0}^{t_0} I dt = \dfrac{1}{R}\int_{0}^{t_0} V(t)\ dt \end{align}$$

である。積分区間は信号の立ち上がり時間を$0\ \mathrm{ns}$として、$t_0\ \mathrm{ns}$まで積分している。あとはこれを解いて$V_\mathrm{max}$を求めればよい。

$$\begin{align} Q = \dfrac{V_\mathrm{max}}{R}\int_{0}^{t_0} \exp \left({-\frac{t}{\tau}} \right)\ dt \end{align}$$

より、

$$\begin{align} Q = \dfrac{V_\mathrm{max}}{R} \left[-\tau \exp \left({-\frac{t}{\tau}} \right) \right]_{0}^{t_0} \end{align}$$

であるから、

$$\begin{align} V_\mathrm{max} = \dfrac{R Q}{\tau (1+e^{-t_{0}/\tau})} \end{align}$$

となる。さてここでわかるパラメータは以下の手段で決定できると考えられる。

| パラメータ | 決定方法                                             |
| ---------- | ---------------------------------------------------- |
| $R$        | ターミネータの抵抗値 = 50 Ω                          |
| $Q$        | 電荷量 (計算の結果得られる既知の量)                  |
| $\tau$     | 信号の時定数。(計算の結果得られる既知の量)           |
| $t_{0}$    | 全電荷の積分区間(プログラム側で決めるものだから既知) |

さて、単純な計算であったが、実際にあっているか確かめてみることにしよう。

## 解析

ということでこのモデルがどれくらいあっているかを調べたいので、既知の波形をオシロで取得してきた。実際に解析してみよう！

### 波形からわかること

以下に取得した波形を示す。この波形から直接振幅の値を読み取っておこう。

<figure markdown>
![](../image/physics/FitWave0.png#center){width=500}
<figcaption>NaI(Tl)のシンチレーション波形。青点線は信号の立ち上がり、赤線はFitの曲線を表す。</figcaption>
</figure>

赤のバツ印は信号が立ち上がった時間(0 ns)、青の点が解析プログラムから求まるピークの最大値(-33 mV)である。モデルの式から $p0 \exp(-t/p_2)+p_3$でfitした。モデルになかった定数項はベースラインが浮いていることを考慮している。Fitの結果得られたパラメータはこちら

```
Paras = [-36.32120691 340.3903735 1.79931561]
ParErr = [ 1.76923651 36.08182273 0.80126617]
```

つまり

$$\begin{align} 
p_0 = V_\mathrm{max} &= -37 \pm 2\ \mathrm{mV} \\ 
p_1 = \tau &= 340 \pm 40 \ \mathrm{ns} \\ 
p_2 = V_\mathrm{baseline} &=  1.8 \pm 0.8\ \mathrm{mV} \\
\end{align}$$

が、この波形が持つ最確値である。(言い方は悪いが、)今回はこの波形がもつ振幅の最大値、時定数の答えだと思っておこう。

### 実際にデータ解析をしたときに得るパラメータを求めておく

さて、次は私達が実際に解析したときにで得るパラメータである、全電荷量と時定数を求めよう。波形の全電荷量$Q$は信号の立ち上がりを$0$ nsとして、$t_0 = 1200$ nsまで積分したものとした。よって、

$$\begin{align} Q &= \int_{0}^{1200} \dfrac{V(t)}{R} dt \\ &= -\dfrac{1}{50\ \mathrm{\Omega}}\sum_{i=0}^{1200} \left(V_i (t_i)-\mathrm{Baseline} \right)\times 10\ \mathrm{ns} \end{align}$$

である。Baselineは信号の立ち上がり時間(0 ns)から前の全点を足し合わせた平均、

$$\begin{align} \mathrm{Baseline} = \sum_{i <=0} V_i (t_i)\ [\mathrm{mV}] \end{align}$$

で計算することができて、結果$Q=199.2$ pCと求まった。信号の時定数は波形を一つ一つFittingしていけば求めることが可能であるが、大変であるし、エネルギーが低くなるとFitが非常に難しい。そこで立ち上がり時間からの経過時間に対して、各時間における信号振幅で重み付けした加重平均である平均時間$\left<t\right>$、

$$\begin{align} \left<t\right> = \dfrac{\sum_{i =0}^{1200} V_i (t_i) t_i}{\sum_{i =0}^{1200} V_i (t_i)}\ [\mathrm{ns}] \end{align}$$

で代用する。計算の結果$\left<t\right>=288.6$ nsという値を得た。[^3]

[^3]:
    fittingの結果と比べるとこちらのほうが小さく見えるが、Fittingのパラメータの誤差の範囲内なので気にしなくて良いだろう。

### モデルが正しいかを検証しよう

さて、これで最確値と実際の実験で得るパラメータ両方が揃ったので、あとは計算して合うかを調べるだけである。これらのパラメータからピークの最大値は、

$$\begin{align} V_\mathrm{max} = \dfrac{50\ \mathrm{\Omega}\times 199.2\ \mathrm{pC}}{　288.6\ \mathrm{ns} \times (1+e^{1200\ \mathrm{ns}/288.6\ \mathrm{ns}})} = 35.1\ \mathrm{mV}\end{align}$$

となり、符号を考慮すれば$V_\mathrm{max} = -35.1\ \mathrm{mV}$となり、プログラムで求めたピークの最大値と概ね一致しそうである。実際fittingによるピーク最大値に対して誤差の範囲内となっている。

ということで電荷スペクトルから、ピークの電荷量orエネルギーに対して得られた電圧値をプロットすれば現状のGainで最大どの程度のエネルギーまで測定可能なのかを見積もることができる。

## 少しだけ誤差も考慮してみた

ここからさらに誤差を考慮することは可能である。まぁ「大雑把」に調べる上で誤差を考える意味があるのかという問題はあるが…

電荷スペクトルのピークをfitすれば、ピークの中央値とその誤差$Q+\sigma_{Q}$と、平均時間の分布から得られる平均時間の最確値と誤差$\left<t\right>\pm\sigma_\mathrm{\left<t\right>}$が得られるわけであるから、$V_\mathrm{max}$にはそれらの誤差が伝搬すると考えられる。このときピーク最大値の誤差$\sigma_{V_\mathrm{max}}$は誤差伝搬の法則より、

$$\begin{align} {\sigma_{V_\mathrm{max}}}^2 = \left(\dfrac{\partial V_\mathrm{max}}{\partial Q} \right)^2 {\sigma_Q}^2+ \left(\dfrac{\partial V_\mathrm{max}}{\partial \left<t\right>} \right)^2 {\sigma_\mathrm{\left<t\right>}}^2 \end{align}$$

である。ここで、

$$\begin{align} 
\dfrac{\partial V_\mathrm{max}}{\partial Q} &= \dfrac{R}{\left<t\right> (1+e^{-t_{0}/\left<t\right>})} = \dfrac{V_\mathrm{max}}{Q}\\
\dfrac{\partial V_\mathrm{max}}{\partial \left<t\right>} &= RQ \dfrac{d}{d \left<t\right>} \dfrac{1}{\left<t\right> (1+e^{-t_{0}/\left<t\right>})} \\
&= \dfrac{ V_\mathrm{max}}{\left<t\right>} \left[ 1 + \dfrac{t_0}{\left<t\right>} \dfrac{e^{-t_0/\left<t\right>}}{1+e^{-t_0/\left<t\right>}} \right]
\end{align}$$

なので、

$$\begin{align} \sigma_{V_\mathrm{max}}= \sqrt{\left(\dfrac{V_\mathrm{max}}{Q}\sigma_Q \right)^2 + \left[ \dfrac{ V_\mathrm{max}}{\left<t\right>} \left\{ 1 + \dfrac{t_0}{\left<t\right>} \dfrac{e^{-t_0/\left<t\right>}}{1+e^{-t_0/\left<t\right>}} \right\} \sigma_\mathrm{\left<t\right>} \right]^2 } \end{align}$$

となる。
