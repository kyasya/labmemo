---
description: 熱運動をマクロの視点から見てみよう
image: image/card/temp.png
---

# 気体分子運動論(古典熱力学)

![](../image/card/temp.png#center){width=600}

高校でも習うが、大学の化学、物理における熱系の講義で必ずやるくらい重要なモデル。

# 気体分子運動論

気体分子運動論とは、気体の熱運動を古典物理学(マクロ)の視点から説明するモデルの一つで、高校物理では名前こそあまり出てこないものの、1節をかけてじっくり学ぶ重要なモデルとなっている。[^1]

[^1]:
  私の記憶では内部エネルギーの直前に習った。

今講義では

1. 内部エネルギーの話(エネルギー等分配の法則)
2. 古典論における熱運動の速度分布(Maxwell-Boltzmann分布)

まで議論することとする。

ここで下図の左側に示すように一辺が$L$の立方体の容器に理想気体が封入されているとして議論する。ただし以下の条件

- 気体分子は単一の種類の理想気体
    - 分子同士で引・斥力などの相互作用を起こさない
    - 分子の大きさを考えない(ただの点とする)
- 封入されている気体の総量$N$は1 mol
- 気体分子は壁と弾性散乱する
- 高校物理と違って最初から速度分布を考慮して計算する

を仮定して議論をすすめることにしよう。実際の気体は複数種類の気体からなる混合物であるし、気体分子間の相互作用もあるが、計算が複雑になるためである。
<!-- また、これらの仮定の下でも気体の運動をしっかり説明できる。 -->
また、高校物理では「気体分子の速度のばらつきがあるから…」ということで速度を平均速度に置き換えるという操作をすることで導出するが、統計学の知識を少しつかってより厳密に考えてみることにしよう。

<figure markdown>
![](../image/physics/kinetic_theory_of_gases.png#center)
<figcaption>気体の入った立方体の容器(左)と、x軸方面のある気体分子に注目した様子(右)</figcaption>
</figure>

## まずは1粒子かつ1方向の運動を考える

容器内の気体分子たちはあらゆる方向にあらゆる確率でランダムに運動している。気体分子は速度$\boldsymbol{v}=(v_x, v_y, v_z)$を持ち、壁面で弾性衝突する。ここで気体分子の速度はある程度のばらつきを持つことに注意して計算を進めることにする。

ただし一つの気体分子とはいえ、一気に全方向の速度に対する運動を考えるのは大変であるからまずは1成分だけを考えよう。ある位置成分、上の図ではx軸方向の速度成分についてだが、気体x軸方向の速度を調べ、横軸を速度としてグラフを書いたとき、その速度分布の形が$f(v_x)$という関数で表せたとする($f(v_x)$の具体的な形については後述)。
ここで速度分布関数$f(v_x)$は統計学における確率密度関数であるため、速度が$v_x$と$v_x + dv_x$の間にある確率(粒子の数)は密度×幅なので、$f(v_x) d v_x$と書ける。

まず一成分である$x$軸方向に対する気体分子の運動において、気体分子が壁面に与える運動量$\Delta p_x$は

$$\begin{align}\Delta p_x  = mv_x + mv_x = 2 m v_x\end{align}$$

である。この後、弾性散乱した粒子は反対側にあるx軸に垂直な壁面に再び衝突し、またこちらに戻ってくるというのを繰り返していると考えられる。つまり単位時間あたりに1つの壁面に衝突する回数は速度を壁を往復する距離で割ればいいので$v_x/(2L)$とかける。これらから単位時間(1 sec)あたりに壁面が受ける力積を計算することができて、

- 1気体分子が1回壁に衝突したときに与える力積 = 運動量の差分: $2 m v_x$
- 単位時間あたりに1つの壁面に衝突する回数 = $v_x/(2L)$
- 分子の総数 = (気体分子の総数) $\times$ (粒子が微小な速度区間に所属する確率) = $Nf(v_x) dv_x$
 
これらを掛け合わせたもの、

$$\begin{align}2 m v_x \times \frac{v_x}{2L} \times Nf(v_x) dv_x \Delta t\end{align}$$

が壁面が**微小な速度分布の区間$dv$において、すべての粒子から壁面が**受ける力積となる。ここで単位時間あたりであるので$\Delta t = 1$より、

$$\begin{align}\frac{m}{L}Nf(v_x)v_x^2 dv_x \end{align}$$

と書ける。故に**粒子の速度分布全体における**全粒子から壁面が受ける力$F_x$は気体分子の速度分布の全区間に対して積分すればよい。故に積分区間は静止状態からどれくらい早く運動してもよいので、

$$\begin{align}F_x = \frac{m}{L}N \int_0^\infty f(v_x)v_x^2\ dv_x\end{align}$$

となる。圧力$P_x$は、立方体の容器をを考えているので任意の面の表面積が$L^2$であることから

$$\begin{align} P_x = \frac{F_x}{L^2} = \frac{mN \int_0^\infty f(v_x)v_x^2\ dv_x}{L^3} = \frac{mN \int_0^\infty f(v_x)v_x^2 dv_x}{V}\end{align}$$

となる。ここで$V$は容器の体積である。ここで

$$\begin{align} \left<v_x^2\right> = \int_0^\infty v_x^2f(v_x)\ dv_x\end{align}$$

は統計学の平均の式から速度の二乗$v_x^2$の平均値$\left<v_x^2\right>$であることから、

$$\begin{align} P_x = \frac{Nm}{V}\left<v_x^2\right>\label{eq:p_x}\end{align}$$

とかける。

## 次に全方向(粒子の速度ベクトル)を考える

前節で容器の1方向における圧力が計算できた。ここで気体分子は非常に軽く**重力の影響は無視できるため**、気体分子は重力の影響を受けず全方向に対して同じ確率で運動している[^2]。つまり分布もすべて同じ形、
[^2]:
    数学的に言うと、運動が方向の分布の確率は同様に確からしいということだね。

$$\begin{align} f(v_x) = f(v_y) = f(v_z) \end{align}$$

であるから、

$$\begin{align} \left<v_x^2\right> = \left<v_y^2\right> = \left<v_z^2\right> \end{align}$$

となる。速度の二乗平均を全て足せはベクトルの足し算的に粒子の速度二乗平均$\left<v^2\right>$になる、

$$\begin{align} \left<v^2\right> = \left<v_x^2\right> + \left<v_y^2\right> + \left<v_z^2\right> \end{align}$$

故に、粒子の速度平均を使えば各成分の二乗平均は

$$\begin{align} \left<v_x^2\right> = \left<v_y^2\right> = \left<v_z^2\right> = \frac{1}{3}\left<v^2\right>　\label{eq:v_sq} \end{align} $$

と書けるので、$\eqref{eq:p_x}$式から、

$$\begin{align} P V = \frac{1}{3}mN\left<v^2\right>\end{align}$$

となる。ここで気体分子の運動エネルギー$\frac{1}{2}m\left<v^2\right>$から、

$$\begin{align} P V = \frac{2}{3}N\frac{1}{2}m\left<v^2\right>\end{align}$$

となり、理想気体の状態方程式$PV=RT$(容器内は1 molの理想気体が入っているので$n=1$)と比較すると、

$$\begin{align} P V = \frac{2}{3}N\frac{1}{2}m\left<v^2\right> = RT\end{align}$$

となるので気体分子の熱運動のエネルギー$\varepsilon$は

$$\begin{align} \varepsilon = \frac{1}{2}m\left<v^2\right> = \frac{3}{2} \frac{R}{N} T = \frac{2}{3} k_B T \label{d-energy}\end{align}$$

となる。ここで$k_B=R/N$は**ボルツマン定数**と呼ばれる量である。ここで温度$T$に対して変形すると、

$$\begin{align} T = \frac{2}{3} \frac{1}{k_B} \varepsilon\end{align}$$

となり**絶対温度は気体分子の熱運動のエネルギーに比例する**事がわかる。この比例の関係によって温度計のメモリは均等に振っても大丈夫というわけである。加えて式の形から以下のことがいえる。

- 温度が熱運動の激しさ(エネルギー)を表す指標となっていること
- 温度の低下によって、熱運動の激しさは小さくなる
  - **古典論において**、$T = 0$ Kで粒子の熱運動は完全に停止する。この温度は**絶対零度**と呼ばれる
    - ただし、実際は0 Kでも熱運動は止まらない。これは**零点振動**と呼ばれ、量子論で説明される。

よって、容器内の運動エネルギーの総和は$E=N\varepsilon$なので、

$$\begin{align} E = N \frac{3}{2} k_B T = \frac{3}{2} RT\end{align}$$

である。これが容器「内部」の気体分子全ての運動エネルギーとなるので**内部エネルギー**と呼ばれる。また気体分子の二乗平均速度は

$$\begin{align} \left<v^2\right> = \frac{3RT}{Nm} = \frac{3RT}{M} \end{align}$$

となる。$M=Nm$は分子量を表す。

# エネルギー等分配の法則

前節の結果から、($\ref{eq:v_sq}$)式より気体分子の運動エネルギーは

$$\begin{align} \frac{1}{2}m\left<v_x^2\right> = \frac{1}{2}m\left<v_y^2\right> = \frac{1}{2}m\left<v_z^2\right> = \frac{1}{3}\frac{1}{2}m\left<v^2\right> \end{align}$$

と書けるから、

$$\begin{align} \frac{1}{3}\frac{1}{2}m\left<v^2\right> = \frac{1}{3}\frac{3}{2}k_BT = \frac{1}{2}k_BT\end{align}$$

となる。この結果が示すことは1成分に対して$\frac{1}{2}k_BT$の内部エネルギーを持っているということである。つまり

**粒子の自由度1つにつき$\frac{1}{2}k_BT$のエネルギーが分配される**

ということができ、この法則を**エネルギー等分配の法則**(equiparttition law)という。**自由度**とは物体がどのような方向等に運動できるかを表す指標である。例えば今まで議論した気体分子は3次元の範囲内で並進運動(移動する運動のこと)をするので自由度は3である。

### エネルギー等分配の例

これが例えば2原子分子気体だったなら、3次元の並進運動に加えて二方向に回転運動もするので自由度は

$$\begin{align} (並進: 3次元)+(回転: x,yの2次元) = 3+2=5\end{align}$$

である。故に

$$\begin{align} 5 \times \frac{1}{2}RT = \frac{5}{2}RT\end{align}$$

の内部エネルギーを持つ。内部エネルギーが計算できれば定積比熱$C_\mathrm{v}$と定圧比熱$C_\mathrm{p}$が計算できて、

$$\begin{align} C_\mathrm{v} = \frac{5}{2}R,\ C_\mathrm{p}= C_\mathrm{v}+ R = \frac{7}{2}R\end{align}$$

となるので比熱比$\gamma$は

$$\begin{align} \gamma = \frac{C_\mathrm{p}}{C_\mathrm{v}} = 7/5 = 1.4 \end{align}$$

と計算できる。

# Maxwell-Boltzmann分布

## 導出

さて、気体分子の速度分布の形を明らかにしよう。ここで各成分に対して微小な速度区間全て対して、速度の微小区間が作る微小な立方体(体積素片)に所属する粒子の速度分布$F(v_x, v_y, v_z)$を考えればよい。あとは1成分と同じく、その体積素片、つまり

$$\begin{align} \begin{pmatrix}v_x\\v_y\\v_z\end{pmatrix} \to \begin{pmatrix}v_x+dv_x\\v_y+dv_y\\v_z+dv_z\end{pmatrix}\end{align}$$

の区間中の速度分布に対して粒子が所属する確率は

$$\begin{align} F(v_x, v_y, v_z) dv_x dv_y dv_z \label{eq:Fvdv}\end{align} $$

である、あとは$F(v_x, v_y, v_z)$の具体的な形さえわかれば全速度分布の成分を積分すれば粒子内の全速度に対する速度分布がどうなっているかわかるだろう。
しかしこの$F(v_x, v_y, v_z)$を一気に解くのはかなり難しそうなので分解して考えることにする。つまり**各成分の速度分布は独立している**とし、速度分布$F$は各成分の速度分布$f$の積で表されると仮定する。

$$\begin{align} F(v_x, v_y, v_z) = f(v_x)f(v_y)f(v_z)=F(v) \label{eq:inv_f}\end{align} $$

ここで$F(v)$と置けるのは前述したように気体がランダムに運動しているため、気体分子は純粋に自身が持つ速度$v$のみに依存していると考えられるからである。ここで両辺に対数をとれば

$$\begin{align} \ln F(v) = \ln f(v_x) + \ln f(v_y) + \ln f(v_z)\end{align} $$

この両辺を各位成分で微分する。1成分だけやっておこう

$$\begin{align} \dfrac{\partial }{\partial v_x} \ln F(v) &= \dfrac{\partial }{\partial v_x} \ln f(v_x) \\ \frac{v_x}{v} \frac{F'(v)}{F(v)}&=\frac{f'(v_x)}{f(v_x)} \end{align} $$

よって、

$$\begin{align} \frac{v_x}{v} \frac{F'(v)}{F(v)}=\frac{f'(v_x)}{f(v_x)}\\ \frac{v_y}{v} \frac{F'(v)}{F(v)}=\frac{f'(v_y)}{f(v_y)}\\ \frac{v_z}{v} \frac{F'(v)}{F(v)}=\frac{f'(v_z)}{f(v_z)}\end{align} $$

なので変形すれば

$$\begin{align}  \frac{F'(v)}{vF(v)}=\frac{f'(v_x)}{v_x f(v_x)} =\frac{f'(v_y)}{v_y f(v_y)}=\frac{f'(v_z)}{v_z f(v_z)}=-2A\end{align}$$

となるが、各速度分布が独立であるため、この式が成立するためにはこれらは定数でなければならなず、$-2A$とおいた。この理由は実際に定数の部分を別の速度成分でおいて計算すればよい。例えば$\frac{F'(v)}{vF(v)}=v_x$が成立すれば各成分の独立性が失われるのは用意にわかる。また$-2A$中途半端な置き方に見えるが、これは今後の計算を簡略化するためなので我慢してほしい。
ここから、

$$\begin{align}  \frac{f'(v_x)}{f(v_x)} = -2A v_x,\ \frac{f'(v_y)}{f(v_y)}= -2A v_y ,\ \frac{f'(v_z)}{f(v_z)}=-2Av_z\end{align}$$

とかける。あとは積分して微分方程式を解こう。1成分$v_x$について、

$$\begin{align}  \int \frac{f'(v_x)}{f(v_x)} dv_x &= -2A \int v_x dv_x \notag \\ \ln f(v_x) &= -A{v_x}^2 + C\ (C=\mathrm{const.}) \notag \\ f(v_x) &= B \exp(-A{v_x}^2)\ (B=e^C=\mathrm{const.})\end{align}$$

から、残りの成分も同様に

$$\begin{align} f(v_x) &= B \exp(-A{v_x}^2)\\f(v_y) &= B \exp(-A{v_y}^2)\\f(v_z) &= B \exp(-A{v_z}^2)\end{align}$$

とかけるなのでこれを$\eqref{eq:inv_f}$式に代入すれば

$$\begin{align} F(v) = B^3 \exp\left[-A({v_x}^2 + {v_y}^2 + {v_z}^2) \right]\end{align} $$

となる。さあ、ここまで来たらあとは定数$A$、$B$を決定すれば$f(v)$が特定できそうだ。まず、$f(v)$は確率密度関数なので全区間を積分すれば1 (100 %)になる。よって、

$$\begin{align} \int_{-\infty}^{\infty} f(v_x) dv_x &= B \int_{-\infty}^{\infty} \exp(-A{v_x}^2) dv_x = 1\end{align}$$

だから、Gauss積分の公式

$$\begin{align} I = \int_{-\infty}^{\infty} \exp(-Ax^2) dx = \sqrt{\frac{\pi}{A}}\end{align}$$

をつかえば、

$$\begin{align} B \sqrt{\frac{\pi}{A}} = 1 \notag \end{align}$$

なので

$$\begin{align} B = \sqrt{\frac{A}{\pi}} \end{align}$$

となり$A$、$B$の関係性が求まった。あとは$A$を決定しよう。ここでGauss積分の両辺を微分した

$$\begin{align} \int_{-\infty}^{\infty} Ax^2\exp(-Ax^2) dx &= \frac{1}{2} \sqrt{\frac{\pi}{A^3}}\end{align}$$

を使用する。統計学における平均値の定義から平均値$\left<v^2\right>$は、

$$\begin{align} \left<v^2\right> =  \dfrac{\int_{-\infty}^{\infty} {v_x}^2 f(v_x) dv_x}{\int_{-\infty}^{\infty} f(v_x) dv_x}=\dfrac{\int_{-\infty}^{\infty} {v_x}^2\exp(-Ax^2) dv_x}{\int_{-\infty}^{\infty} \exp(-Ax^2) dv_x}=\frac{1}{2A}\end{align}$$

となる。この結果をエネルギー等分配の法則に代入して定数$A$を決定できて、

$$\begin{align} \frac{1}{2}k_B T = \frac{1}{2} m \times \frac{1}{2A}\notag \\ A = \frac{1}{2} \frac{m}{k_B T}\end{align}$$

となる。よって1成分における速度分布が

$$\begin{align} f\left( v_{x}\right) =\left[ \dfrac{1}{2\pi }\dfrac{m}{k_{B}T}\right] ^{1/2}\exp \left[ -\dfrac{m}{2k_BT}{{v_x}^2}\right] \end{align}$$

であることがわかった。残りの成分も同様の形になるから($\ref{eq:inv_f}$)式に代入すれば、

$$\begin{align} F\left( v\right) &=\left[ \dfrac{1}{2\pi }\dfrac{m}{k_{B}T}\right] ^{3/2}\exp \left[ -\dfrac{m}{2k_BT}\left({v_x}^2+{v_y}^2+{v_z}^2\right)\right] \notag \\
&=\left[ \dfrac{1}{2\pi }\dfrac{m}{k_{B}T}\right] ^{3/2}\exp \left[ -\dfrac{m}{2k_BT}{v}^2\right]\\
\end{align}$$
<!-- &=\left[ \dfrac{1}{2\pi }\dfrac{m}{k_{B}T}\right] ^{3/2}\exp \left[ -\dfrac{\varepsilon}{k_BT}\right] -->
となって$F\left( v\right)$の形が定まった。あとは全粒子の運動における速度分布$g(v)$を知ることができれば完璧だ。
これは粒子がある速度$v$で運動しているとき、その速度が$v$−$v+dv$の領域にいる確率$g(v)dv$を考えればよい。さて$g(v)$を求めるために$\eqref{eq:Fvdv}$式を全速度空間の領域で積分しよう。

$$\begin{align} \int_{-\infty}^{\infty} \int_{-\infty}^{\infty} \int_{-\infty}^{\infty} F(v) dv_x dv_y dv_z \end{align} $$

ここで各気体分子はランダムに運動しているのだったから、各成分の速度を軸とした速度空間を考えたときその速度ベクトルの分布は球形となる。ということで球全体に対して積分を行うことになるが、$dv_x dv_y dv_z$はデカルト座標系なので球の計算がしやすいように球座標に座標変換、

$$\begin{align} = \int_{-\pi}^{\pi} \int_{0}^{\pi} \int_{0}^{\infty} F(v) v^2 \sin \theta dv d\theta d\phi \end{align} $$

を行って積分を実行する。ここでも各変数が独立していると考えられるので成分ごとの積で計算できて、

$$\begin{align} &=  \int_{0}^{\infty} F(v) v^2 dv \int_{0}^{\pi}\sin \theta d\theta \int_{-\pi}^{\pi} d\phi \notag \\ &= \int_{0}^{\infty} F(v) v^2 dv \times \left[\sin \theta\right]_{0}^{\pi}  \times \left[\phi\right]_{-\pi}^{\pi} \notag \\ &= \int_{0}^{\infty} 4\pi v^2F(v) dv\end{align} $$

となる。故に

$$\begin{align} 4\pi v^2F(v) dv &= 4\pi v^2 \left[ \dfrac{1}{2\pi }\dfrac{m}{k_{B}T}\right] ^{3/2}\exp \left[ -\dfrac{m}{2k_BT}{v}^2\right]dv \notag \\ &= g(v) dv \end{align}$$

から、

$$\begin{align} g(v) &= 4\pi v^2 \left[ \dfrac{1}{2\pi }\dfrac{m}{k_{B}T}\right] ^{3/2}\exp \left[ -\dfrac{m}{2k_BT}{v}^2\right]\label{eq:maxwell} \end{align} $$

となり、これが**Maxwell-Boltzmann分布**である。統計力学ではさらに、($\ref{d-energy}$)式と逆温度$\beta=\frac{1}{k_B T}$を使って[^3]、

[^3]:
  温度の逆数なので逆温度と呼ばれる。

$$\begin{align}4\pi v^2 \left[ \dfrac{\beta m}{2\pi }\right] ^{3/2}\exp \left[ -\beta \varepsilon v^2\right]\end{align}$$

の形になったものをよく見ることになる。ポンと式を出されてもイメージが付きにくいだろうから温度が上昇していくにつれての速度分布がどのように変化するか描いてみよう。$\eqref{eq:maxwell}$式を見てみると、変数は$v$のみで残りはすべて定数なのでこれらのパラメータを自分で設定して雑にPythonでプログラムを書いてみた[^4]。

[^4]:
  本当に雑で笑う。みんなは定数の定義とかに気を付けよう！

- 質量$m$: 酸素の質量を仮定。アボガドロ数で割って、gをkgに変換して次元合わせ
- 温度$T$: 低温から高温まで複数のパラメータを設定。

とした。

??? note "Pythonソースコード"
    ```python
    #%%
    import matplotlib.pyplot as plt
    import scipy as sc
    import numpy as np
    import matplotlib.animation as animation
    from mpl_toolkits.axisartist.axislines import SubplotZero

    plt.rcParams["font.size"] = 26 #フォントの大きさ

    kB = 1.380649e-23 # boltzmann const [J/K]=[m^2*kg*/(s^2)/K]:
    m = 16*2/1000/6.02e23 # kg = g/1000 [g/kg]/mol^-1 @ 16 Oxygen

    def MaxwellBoltzmann(v, T):
        return 4*np.pi*v**2*(m/(2*np.pi*kB*T))**(3/2)*np.exp(-v**2*m/(2*kB*T))

    def PlotMaxwellBoltzmannGif():
        fig, ax = plt.subplots(1, 1, figsize=(12,9))

        v = np.arange(0, 1100, 0.01) # velocity of gas
        ax.set_xlim(min(v), max(v))

        ims = []
        m = max(MaxwellBoltzmann(v,  50)) # normalizing const
        ax.set_xlabel('$v$ [m/s]', loc='right')
        ax.set_ylabel('Events (normalized) [a.u.]', loc='top')
        TList = [50, 100, 273, 400, 600, 800, 1000]
        for t in TList:
            ThisPlot = ax.plot( v, MaxwellBoltzmann(v, t)/m, label=f'{t} K', linewidth=2)
            ax.legend()
            ims.append(ThisPlot)

        ani = animation.ArtistAnimation(fig, ims, interval=500, blit=True, repeat_delay=1000)

        plt. subplots_adjust(left=0.1, right=0.95, bottom=0.1, top=0.95)
        ani.save('Maxwell-Boltzmann.gif', dpi=40, writer="pillow")

    PlotMaxwellBoltzmannGif()
    ```

結果が下図である。[^5] 縦軸は粒子の確率を表す(ただし50 Kピークの最大値で規格化している[^6])。

<figure markdown>
![](../image/physics/Maxwell-Boltzmann.gif#center)
<figcaption>酸素分子の場合のMaxwell-Boltzmann分布</figcaption>
</figure>

[^5]:
  実際の酸素分子は2原子分子で、前述したように自由度が異なるし、そもそも分子でなので理想気体のように一つの粒子でもないので正確には「酸素分子と同じ質量を持つ理想気体」と言うべきでだろうけど、まぁいいでしょ。ｷﾆｼﾃﾙﾋﾄﾅﾝﾃｲﾅｲﾖ…
[^6]:
  規格化。何かしらの最大値を1(100%)になるように調整すること。これで最大値にたいしてどれくらいの速度を持っているかを割合であらわせる。今回はは50 Kの速度分布の最大値が1になるようにそのピークの最大値で各速度を割っている。

これからわかるように温度の上昇に伴って

- 熱運動の速度が上がる。
- 速度の分布も広範囲となる。

ということがわかる。これは個体から液体、気体へと状態変化するにつれて熱運動は激しくなるというのイメージに一致する。熱運動が激しいということは温度上昇に伴って気体分子の速度がどんどん早くなるということである。また気体のような高温では気体分子があらゆる速度を持って運動している。50 K(常圧力で固体)だと速度分布が狭くなっていることがわかる。これは気体分子(固体だが)がぎゅっと集まって熱運動していることを表しており、これは固体における熱運動のイメージとも一致していることがわかる。
これらの結果からこのMaxwell-Boltzmann分布が気体分子だけでなくあらゆる物体の状態について古典的な熱運動のイメージを説明できることがわかるだろう。[^7]

[^7]:
  今先から古典古典とうるさいが、実は古典論では全ての粒子が区別可能であるとして議論をしている。量子論や統計力学によるとさらに粒子の区別ができるかできないかで粒子の運動が変わる。結論から言うとフェルミ分布とボース分布という二種類の統計分布があり、フェルミ分布は化学でいう共有結合(結合の時に電子は2つで一つのペアを作るのか)に、ボース分布は超伝導などにかかわってくる。

## 重要な指標

Maxwell-Boltzmann分布から、分布のピークが最大となるピークの中央値、**最多速度**$v_m$とピークの平均値$\left<v^2\right>$を計算しよう。

最多速度はピークが最大となる場所なので分布関数を速度で微分して0となる場所を捜せばよい。

$$\begin{align}\dfrac{dg(v)}{dv} = 2v \exp \left[-\frac{m}{2k_BT} v^2\right] - \frac{m}{k_B T}v^3 \exp \left[-\frac{m}{2k_BT} v^2\right] = 0\end{align}$$

故に

$$\begin{align}v_m = \sqrt {\frac{2k_BT}{m}} = \sqrt {\frac{2RT}{M}} \end{align}$$

である。また平均値は

$$\begin{align}\left<v^2\right> = \int_0^\infty v g(v) dv = \frac{1}{\sqrt{\pi}} \sqrt {\frac{2k_BT}{m}} = \frac{1}{\sqrt{\pi}} \sqrt {\frac{2RT}{M}} = 1.1284v_m\end{align}$$

同じ考えで二乗平均も計算できる。

$$\begin{align}\left<v^2\right> = \int_0^\infty v^2 g(v) dv = \frac{3k_BT}{m} = \frac{3RT}{M} \end{align}$$

ここから二乗平均速度の平方根を計算できて、

$$\begin{align} \sqrt{\left<v^2\right>} = \sqrt{\frac{3}{2}} \sqrt {\frac{2k_BT}{m}} = 1.2247v_m \end{align} = \sigma_v$$

となる。これは物理学では**根二乗平均速度**または平均速度の**ゆらぎ**と呼ばれている。これは統計学でいうMaxwell-Boltzmann分布における速度の**分散**のことである。
