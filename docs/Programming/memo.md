---
description: C++の便利機能&沼どころさん
image: image/card/memo.png
---

# 便利な機能

![](../image/card/memo.png#center)

C++の便利機能&沼どころさん

## 独自の型を定義したい、長い変数宣言を省略したい。

### `typedef`や`using`を使う。

**型名と言っているが、クラスなどでもおｋ**

`typedef`の場合

```c++
typedef [既存の型名] [新しい型名];
```

`using`の場合

```c++
using [新しい型名] = [既存の型名];
```

どっちでも基本は同じ。例えばdouble型でx,y,zの3成分のベクトル扱う`KVector<型名>`なんてクラスがあったとき。typedefなら

```c++
typedef KVector<double> KVecD;
```

というように書く。ここで`D`は`double`方であることを表す。`using`であれば

```c++
using KVecD = KVector<double>;
```

である。

### 便利ポイント

こいつの便利ポイントは3つある。
そのうち2つは**よりわかりやすい型名を定義できる**ことによるものだ。

1. よりわかりやすい型名を定義できる: **長い名前を短く**
   前節ではあまり恩恵がなさそうに見えるが、真っ先に思いつくのが長い型名(クラス名)を短くできることである。例えば`std::vector`で２次元配列を作るときはクソ長くなるが、それを`Matrix`型と称して、

    ```c++
    typedef std::vector<std::vector<double>> MatrixD;
    ```

    とすればクソ長い定義を毎回書かずに済む。

2. よりわかりやすい型名を定義できる: **型に名前をつける**
例えば`int`型はいわば「ただの」整数型である。これにわざわざ`Register`とか名前をつけることによって、これがレジスタ管理を行う変数なんだな...等といった情報をまとめることができる。
また**定義した変数同士はすべて別々の変数型として扱われるため**、プログラムを管理するレジスタ型と通常のint型を足し算するといった事故をしてしまったとしても、コンパイルしたときに教えてくれるようになる。

1. **テンプレートの記述にも対応している**(`using`のみ)
また`using`の場合は。前節で説明したオブジェクトはdoubleならD、intならIというように自分側で毎回名前を作らなければならない。単純なものであればテンプレートで方を指定できる仕様にしたほうが楽だと思うこともあるだろう。そういうときに使える。

```c++
template<typename T>
using KVec = KVector<T>;
```

こうしてとけば、メインプログラムで

```c++
KVec<double> v;
```

というように使える。

### まとめ

- 独自の型を定義できる
- 定義した独自の型は定義元の変数型や元の変数型から派生した別の型とは別物として扱われる(混同するとエラーになる)
- テンプレートを使いたいときは`using`を、そうでないときは`typedef`を使おう

## CMake

最近はMakefileをベタ書きで書くことはなくなったらしい。`CMake`は非常に便利だ。
- [使い方](https://qiita.com/termoshtt/items/539541c180dfc40a1189)
- [便利な機能](https://qiita.com/mrk_21/items/5e7ca775b463a4141a58)