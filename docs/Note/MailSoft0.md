# Mailの自動送受信ソフトを作る

定期的に自身に関係のある論文を探してきてはリストにして手元に送信してくれるソフトを作りたいなとおもったのでまずはメールを送信するプログラムをPythonで書くことにした。
スペックが低い私は論文を描いてたり解析をしているとそっちに付きっきりになって、研究会発表前に泣きながら論文を探したり、教授に聞きに行ったりするのが恥ずかしいので…
あと論文が出版されると参考文献を書き直す必要があるので…毎日チェックすればいいけどめんどくさいので…

後Mailを受信して解析するプログラムもここに載せます。

## メールの送受信の仕組み(メモ)

メールの送受信にあたり次のようなメールの暗号化方式がある。

- 送信: SMTP over SSL
- 受信: POP over SSL

これに合わせてpythonプログラムを構成する必要がある。

### 参考文献
ここに書いてある通りにすればできる。いつかは添付ファイルも送れるようにしたい。

- [【Python】もっとも基本的なメール送信の方法](https://gakushikiweblog.com/python-email)
- [Pythonでのメール送信](https://zenn.dev/shimakaze_soft/articles/9601818a95309c)

### コード
参考文献をもとにほぼ丸コピ

クラス化したくらい

```python
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders

class SendingMail:
	# server
	TempServer = 'smtp.gmail.com' # gmail
	# smtp_server = 'smtp-mail.outlook.com' # Outlook
	Port = 587

	LoginAddress = '' # ログインするメールアドレス(ふつうは送信元のアドレスを使うと思う)
	LoginPassword = '' # 二段階認証のタブから設定したgoogleアプリパスワードを入力

	MailServer = 0

	def __init__(self) -> None:
		pass

	def SetMailServer(self, server, port):
		self.TempServer = server
		self.Port = port

	def ConnectionServer(self, login_addr, login_pass):
		# connect SMTP server
		print("connecting server...")
		self.MailServer = smtplib.SMTP(host=self.TempServer, port=self.Port)

		# Setting TLS(Transport Layer Security)
		self.MailServer.starttls()

		print("done.")
		print("login at email account...")
		# login at SMTP server
		self.LoginAddress = login_addr
		self.LoginPassword = login_pass

		self.MailServer.login(user=self.LoginAddress, password=self.LoginPassword)

		print("done.")

	def SendMessage(self, send_to, send_from='', subject='', text=''):
		print("writing message...")

		# writing message
		message = MIMEMultipart()

		message['To'] = send_to # 送り先

		message['Subject'] = subject # 件名

		if send_from == '': # 送信元
			message['From'] = self.LoginAddress
		else:
			message['From'] = send_from

		# 本文(文字)
		message.attach(MIMEText(text, 'plain'))

		# 添付ファイルがあるなら記述 (今はリファレンスが理解できないのでコメントアウトして放置)
		# files
		#attachment = MIMEBase('semdmail.zip', 'zip')
		#with open('./sendmail.zip', 'br') as f:
		#	attachment.set_payload(f.read())
		#	encoders.encode_base64(attachment)
		#	attachment.add_header()

		# sending message
		print("sending...")
		self.MailServer.send_message(message)

		self.MailServer.quit()


# 実際にgmailをメールを送信してみる(下に簡易な説明)
a = SendingMail()　# コンストラクタ

a.ConnectionServer(login_addr='メールサーバーにログインするためのメールアドレス', login_pass='パスワード')

a.SendMessage(send_to='送信先メールアドレス',
	    	  subject='件名',
		      text='本文')

```

簡単な説明

- `ConnectionServer(login_addr='', login_pass='')`:メールサーバにログインするためのアカウントとパスワードを設定することでメールサーバに接続する。アカウントは基本的に送信元のメールアドレスとパスワード。ただし、今回のように**gmailを使う場合はアカウントの設定->二段階認証->アプリコードで発行されるパスワードを使う**

- `SendMessage(self, send_to, send_from='', subject='', text=''):`:メールを送信する。左から送信先メールアドレス、送信元メールアドレス(空白でメールサーバにログインした時のメアドが使われる)、件名、本文。すべてプレーンテキスト。

気が向くか必要に駆られたら添付ファイルの送信関係のプログラムも追加すると思う。