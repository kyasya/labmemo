# arXiv APIを使って論文検索

## 何故作りたくなったのか

Mailの部分でも書いたけど、スペックが低い私は論文を描いてたり解析をしているとそっちに付きっきりになって、研究会発表前に泣きながら論文を探したり、教授に聞きに行ったりするのが辛いので、定期的に自身に関係のある論文を探してきてメールで教えてくれるソフトを作りたいと思った。

## arXiv
物理(宇宙系)といえば[arXiv](https://arxiv.org/) (アーカイヴ)という論文サイトがある。一般的に出版されるとその出版先のサイトに論文が掲載されるが、その前に査読中(確か)の論文などはまずここに挙げられる。もちろん査読を通していない論文も転がっているため情報の吟味は必要だが、リアルタイムで世界で行われている実験の成果を見ることができるというメリットがある。また、実際に査読が通って出版された論文についてもその論文の掲載先のURLがちゃんと掲載されるのでこのサイトをたどって出版先のサイトに行くことや、逆に出版先のサイトがPDFを無料で公開してない場合にarXiv側でとりあえずの情報を収集できる。

ということでまずは個々に掲載されている論文をざっと検索してリストにすることができれば自分の知りたい論文の大半を見ることができるし、参考文献リストを作るのも楽ということである。ということで問題は私が極端なめんどくさがりかつ、多忙になるとチェックがおろそかになってしまうことである。さら、これからますます忙しくなりそうな雰囲気が漂っているため、卒業で余裕がある今のうちに検索するソフトを開発してしまおうというわけだ。

## ソフトを作る (進捗次第追記)
さらにarXivにはAPIがある。スクレイピングをしようと思っていたがAPIを使えばもっと簡単にプログラムが組めるかも…[arXiv API](https://info.arxiv.org/help/api/index.html)

開発にはPythonを使う。楽だからね…運用するコンピュータはラズパイです。そんなにスペックがいるものではないし、省電力だし…

### ライブラリのインストール

まずはライブラリの更新
```
python3 -m pip install pip --upgrade
```
次にarXiv APIのPythonラッパー(ライブラリ)をインストールする
```
python3 -m pip install arxiv
```
arXivモジュールの使い方は[arxivモジュールの公式サイト](https://pypi.org/project/arxiv/)を参照するといいだろう。

### 実際にデータをとってくる
一旦APIで情報とってくるまでをやる。公式サイトなどを見ると以下のようにすればとりあえず情報をとってきて表示するまではできた
```python
import arxiv
import pandas as pd

# 著者で検索する場合(auを使う)
Result = arxiv.Search(query='ここに検索したい文字列を記入する', max_results=100)

counter = 0
for result in Result.results():
    print("-----------{}------------".format(counter))
    print("title: {}".format(result.title))
    print("authors: {}".format(result.authors))
    print("arXiv URL: {}".format(result))
    print("arXiv DOI: {}".format(result.doi))
    print("submited at {}".format(result.updated))
    counter += 1
```
`rxiv.Search(query, max_results)`で検索文字列と検索してくるコンテンツの数の上限を決められるらしい、とりあえず100件にした。検索文字列には`AND`、`OR`を使うことで条件をつけて検索もできる。結果は`Result`に格納されるのでそれを`.results()`関数で最初の結果から最後の結果までをループして取り出して表示させている。

結果は次のような形になった
```
-----------カウンターの番号------------
title: タイトル
authors: [arxiv.Result.Author('著者名1'), arxiv.Result.Author('著者名2'), …]
arXiv URL: アーカイブのURL
arXiv DOI: 論文のURL(出版されていなければarXiv、出版されていたら出版先のサイトが表示されているはず)
submited at : 更新日
```

とりあえず情報の抜き出し方はわかったのでこれをどの形式で保存するかを考えたい…Excelとかの表計算ソフトかSQLとかHDFとかのデータベースを使うか…そもそもauthorsの部分がリストでなにかのクラス?に入ってる状態でテキストに書き出されてしまったのでまずはこれを文字列処理で切り取ってスッキリさせなければ…

## 参考文献
- [arXiv API](https://info.arxiv.org/help/api/index.html)
- [arxivモジュールの公式サイト](https://pypi.org/project/arxiv/)