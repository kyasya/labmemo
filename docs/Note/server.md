---
description: 自宅でWeb鯖(HP/wiki)やらマイクラ鯖やら諸々を立てる
image: image/card/server.png
---

# 自宅でWeb諸々鯖

![](../image/card/server.png#center)

自宅でWeb鯖(HP/wiki)やらマイクラ鯖やら諸々を立てる

## なぜ鯖立てしたのか

色々な理由があるけど主な理由を箇条書きするとこんな感じ

- 自由に鯖立てしたかった 
    - referenceは大学内で公開する予定だったが、申請がめんどくさい
    - 逆に申請しないと学内でしか見れない(外からは直接htmlたちをダウンロードしてみてもらわないと行けない)
    - 見れない/めんどくさいのでみんな見ない→結局ワイに直接質問が来て負担が減らない
- 自由に使えるwikiがほしい
    - 上記と同じ理由
    - プラスアルファとして自分が自由に使えて、一部の共有したい情報を限定で共有できるwikiが作りたかった
- 解析のモニタがほしい
    - これも学外から見たくて
    - DAQ関係の情報や簡易的な解析結果を動的サイトとして見れると楽だよねっていう話
- マイクラ鯖をたてたい
    - 身内で遊びたいと言う話になりまして...
- 家族内部で共有できる鯖がほしい
    - 写真をまとめたり、情報を共有できる場所として

## 用意したもの

ということで用意した機器がこちら

| 機器名   | スペック                   | OS                      | 用途                                                 |
| -------- | -------------------------- | ----------------------- | ---------------------------------------------------- |
| ラズパイ | Raspberry Pi 5 (8 GiB RAM) | RaspberryPi OS (64 bit) | 常駐プログラム用(HP・wiki系、常時実行系、WOL)        |
| 高スペ鯖 | 中古のPC                   | Ubuntu mate 24.04 LTS   | マイクラ用(解析やら大容量ファイル鯖としても使いたい) |
| ルータ   | tp-link AX1800             | -                       | ルータ. VPNが使いたかったのでこれ                    |
|          |                            |                         |                                                      |

NOTE: 高スペ鯖はラズパイに比べてという意味であって、いわゆる最新の高スペックPCという意味ではない。GUIも積んでない。

ラズパイ鯖をHPとして使う理由は単純に**省電力**だから。と言っても最新のモデルなのでそこそこの電力は食うと思うけど、普通のPCに比べたら余裕でマシ。なので最低限である程度軽量なものやら、常駐させて実効するプログラムはここにおいて実行させる仕組み。

## 構成

最終的な構成はこんな感じ(本記事執筆時では途中まで実装済)。

![](../image/memo/server/Net-PC.png#center){width=800}

構成するにあたって次のことに注意した。

- IPアドレス: 鯖に対しては100番台の値を振り分ける
- SSH: ポートは通常の22番を使うが、セキュリティ的に良くない。ルータを使って自宅外からは22番以外のポートからアクセスするように設定した。
- 高スペ鯖: 電気代を最小限にするため、現状マイクラ以外に使用していない高スペ鯖は基本シャットダウン。WOL(Wake On LAN)を使って、使いたいときにラズパイ鯖から起動させる。
- HP: HPは基本Basic認証をつかって、パスワードを知らないと閲覧できないようにする。
    - wikiに関してはdokuwikiで非公開wikiが作れるのでそれを使う。

NOTE: **マイクラ鯖は鯖立てそのものしかしていない。大学からの解析情報モニタも同じく。**それに付随するHP整備はこのあとぼちぼちする(後輩含め遊べるメンバーが多忙&他の整備のほうが優先度が高いため)。解析情報モニタも急ぎではない(ぶっちゃけ現状一つ一つ見ていけばなんとかなっている)ので後回し。研究やら就職が辛くなったらやる。

## ルータの設定

**公開できる状態になった時に逐次追加していく**ようにするのが良い。

まずはIPアドレスを手動で割り当てる1xx番台に自動でIPが割り当てられないように設定(適当)

![](../image/memo/server/router-2.png#center){width=700}


次に動的DNSを割り当てる。これは自宅のルータのグローバルアドレスは定期的に変わるため、変わるためにIPアドレスの設定を変えて自宅に接続しなければならなくなり、めんどくさいから。あとIPアドレスをつかってwebにアクセスするのもめんどくさい(というか普通https://~~.comみたいな感じで接続するだろ)というのもあって、ドメインを用意してIPアドレスの紐づけをNo-IPにやらせる。

![](../image/memo/server/router-1.png#center){width=700}

最後にポート転送。鯖が2つあるので特定のポートごとに家の中のラズパイ鯖か高スペ鯖に転送させる。

![](../image/memo/server/router-3.png#center){width=700}

HTTP関係はラズパイ鯖(101)に、マイクラは高スペ鯖(100)に、sshはポートに応じてラズパイ鯖、高スペ鯖に転送させるようにしている(高スペ鯖へのサービス名はsabaでもなければserverでもなくsavaにしてる)。

## Web鯖のインストール

Web鯖は以下の構成とした

```
/var/www/main
/var/www/pwa
/var/www/dokuwiki
```

これらのディレクトリにはindex.htmlやら.phpが入っており、webを開いたときは最初にそこを参照しに行くという仕組みである。これらのディレクトリとドメイン名(url)の設定を行う。できるだけ費用を抑えたいのでドメイン名は[No-IP](https://www.noip.com/)を使ってルータのグローバルIPを動的に結びつけた。

```
https://[webサイト名].ddns.net/
```

**今後はこのURLを使って説明する**。ただし今回の鯖の設定は色々やっているので一気にやらず、**まずはwikiのインストール**からやっていき、最終的にリンク集を作ってその都度Aapche2(webサーバソフト)の設定を変えていく方針で行く。

### wiki: dokuwikiをインストール

[公式によるインストール方法](https://www.dokuwiki.org/install:debian)を参考にしてほしい

```bash
$ sudo apt update
$ sudo apt install php libapache2-mod-php php-xml php-json php-mbstring php-zip php-intl php-gd
$ sudo systemctl restart apache2.service
$ wget https://download.dokuwiki.org/src/dokuwiki/dokuwiki-stable.tgz
$ tar xzvf dokuwiki-stable.tgz
$ sudo mv dokuwiki-*a /var/www/html/dokuwiki
$ sudo chown -R www-data:www-data /var/www/html/dokuwiki
```

最後の`sudo chown -R www-data:www-data [ディレクトリ名]`はApacheがwww-dataユーザーでないと実行できないため。ファイルの所有者を書き換えるということをしている。**HPのディレクトリを追加したときは必ずこの作業をすること**。

### referenceのディレクトリを配置する(これは私だけ)

これも`Doxygen`を用いてhtmlとして生成したものがすでにあるのでそれをラズパイにインスコ。ディレクトリ名をライブラリの名前にならって`pwa`という名前にしている。

```bash
sudo cp -rf pwa /var/www/
```

### リンク集のページを作る

ここにあるindex.htmlには残りのpwaとwikiへのリンク集を乗せている。リンクが書いてあるだけなので、静的サイトジェネレータ[mkdocs]()を使って生成している。

作業用ディレクトリ`main`、更にその下に`docs`というディレクトリ用意して、以下のそこに`README.md`というMarkdownを生成して、

```markdown
# link

| link                                      | Memo      |
| ------------------------------------------| --------- |
| [wiki](https://[webサイト名].ddns.net/wiki)| wiki      |
| [ref.](https://[webサイト名].ddns.net/pwa) | reference |

```

とかく。表敬式でURLを書いているだけ。で`main/mkdocs.yml`を作って、

```yaml
site_name: [鯖名]
theme:
  name: material
  palette:
    primary: teal
  font:
    text: Roboto
nav:
- README.md
```

とかいて

```bash
mkdocs build
```

をして生成した`main/sites`を`/var/www`へコピーする。ついでにsitesをmainに名前を変える。

sudo cp -rf main/sites /var/www/main

### Apacheの設定ファイルを作る

最後にWeb鯖に行った時にHPを見れるようにApacheの設定を行う。今回はたくさんのHPがあるので、以下のような工夫をする。

| URL                                 | 参照するディレクトリ | 中身                         |
| ----------------------------------- | -------------------- | ---------------------------- |
| https://[webサイト名].ddns.net/     | `/var/www/main`      | リンク集                     |
| https://[webサイト名].ddns.net/wiki | `/var/www/dokuwiki`  | wiki (アクセス制限有:非公開) |
| https://[webサイト名].ddns.net/pwa  | `/var/www/pwa`       | reference (パスワード付き)   |
|                                     |                      |                              |

wikiではデフォルトのアクセス制限を、pwaの方にはBasic認証を用いてパスワードを設定した。

- パスワード設定の方法: [同じサーバー内の複数サイトにBasic認証をかけた](https://qiita.com/woonotch/items/291a99dc629e9308a7ee)

設定ファイルはこれ

```xml
<VirtualHost *:80>
    ServerAdmin [連絡先メールアドレス]
    ServerName [取得したドメイン名]

    # デフォルトで参照するディレクトリ()
    DocumentRoot /var/www/main

    <Directory /var/www/main>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>

    # /wiki → DokuWiki
    Alias /wiki /var/www/dokuwiki
    <Directory /var/www/dokuwiki>
        Options +Indexes +FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>

    # /pwa → PWAのreferenceへ
    Alias /pwa /var/www/pwa
    <Directory /var/www/pwa>
        Options Indexes FollowSymLinks
        AllowOverride All

        # パスワードをかける(Basic認証の)設定をする
        AuthType Basic
        AuthName "Restricted Access"
        AuthBasicProvider file
        AuthUserFile /etc/apache2/.htpasswd #パスワードが保存してあるファイルへのPath
        Require valid-user
    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/example_error.log
    CustomLog ${APACHE_LOG_DIR}/example_access.log combined
RewriteEngine on
RewriteCond %{SERVER_NAME} =[取得したドメイン名]
RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
</VirtualHost>
```

で、httpsに対応するため(暗号化はしたいよね)、[Certbot](https://certbot.eff.org)でssl認証をもらう。

```bash
sudo certbot --apache -d [ドメイン名]
```

成功と表示されればおｋ。あとはApacheを再起動

```bash
sudo systemctl restart apache2.service
```

## minecraft serverのインストール

[minecraft server 1.20.1 を ubuntu server 22.04 で動作させる](https://qiita.com/Higemal/items/1ed9f2ec3200d6777562#4minecraft-server%E8%A8%AD%E5%AE%9A)

の4節以降を参考にすれば良い。ただし、他のVerのマイクラをインストールしたりすることもあるかもしれないということも考慮して

```bash
mkdir  ~/Server/1.21.4/
```

を作ってそこに`1.21.4`の鯖を入れた。

## 鯖監視ソフト

[Cockpit](https://cockpit-project.org/)を使えば鯖のアクセス情報などを監視できる。機能としては以下のものがある

- Serviceの監視・管理
- アカウンtのの管理
- ネットワークの監視・ファイアーウォールの設定
- システムログの閲覧
- 診断レポート

インストールと使用方法は以下の通り

```bash
sudo apt install -y cockpit
```

cockpitはネットを通して情報を閲覧することができる。今回は**ローカルネットワーク**で使用することを前提とする。まず、ファイアーウォールを通す。ポート番号は`9090`である。

```bash
sudo ufw allow 9090/tcp
```

できたら`https://[鯖のIPアドレス]:9090/`にWebブラウザで接続する。ユーザー名とパスワードを聞かれるが、デフォルトは鯖のUser名とそのパスワードである。

![](../image/memo/server/server-mon1.png#center){width=600}

こんな感じで色んな情報が見れる

![](../image/memo/server/server-mon0.png#center){width=600}


## WOL(Wake On LAN)の設定

高スペ鯖は電力を食うので使用する機会があるとき以外はシャットダウンしておきたい。ただし通常はシャットダウンしてしまうと自分で物理的にスイッチを入れない限り起動できない。だからといってPCをスリープにするのは本来の電気代節約という観点から見れば理想的ではない。そこで外部(ローカルネットワーク)のPCからLANを通じて起動させたい時に起動できるようにする。実はBIOSには**Wake On LAN(WOL)**と呼ばれる機能があり、これを使えばそれが可能となる。今回は

- 起動したいPC: 高スペPC
- 起動コマンドを送る(WOL機能を実行する)PC: ラズパイ

として機能を実行する。

### 実行されるPCの設定(高スペ鯖)

LinuxでWOLを使うためにethtoolをinstall

```bash
sudo apt install ethtool
```

WOLはイーサネット(LAN)を通じて行うのでMACアドレスとインターフェイス名をを調べる必要がある。`ip add show`コマンドを実行する。

```bash
kyasha@kyasha:~$ ip addr show
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host noprefixroute
       valid_lft forever preferred_lft forever
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether [MACアドレス] brd ff:ff:ff:ff:ff:ff
    inet [IPアドレス/サブネットマスク] brd 192.168.0.255 scope global noprefixroute enp3s0
       valid_lft forever preferred_lft forever
    inet6 [IPアドレス/サブネットマスク(IPv6)] scope link noprefixroute
       valid_lft forever preferred_lft forever
```

ここで`enp3s0`がインターフェイス名、`[MACアドレス]`がMACアドレス。後者は起動を命令する時に必要なのでメモしておこう。インターフェイス名を使ってWOLを有効化する。`ethtool [インターフェイス名]`で確認できる。

```bash
kyasha@kyasha:~$ sudo ethtool enp3s0 | grep Wake-on
        Supports Wake-on: pumbg
        Wake-on: d
```

ここでWake-on: **d**となっていれば、WOLは**無効**である。

```bash
sudo ethtool -s enp3s0 wol g
```

でWOLを有効化する。

```bash
kyasha@kyasha:~$ sudo ethtool enp3s0 | grep Wake-on
        Supports Wake-on: pumbg
        Wake-on: g
```

`Wake-on: g`となっていればおｋ。

#### 自動化(失敗)

**以下うまくいかなかったので代替えの方法を使ってる**

WOLは起動を有効化した**次の起動のみ有効化される**仕様である。これだとめんどくさいので起動するたびWOLを有効化するコマンドを実行させる。これに`systemd`を使う。

まずは、サービスの設定ファイルを作ろう。今回は`wolg.service`という名前にした。

```bash
sudo nano /etc/systemd/system/wolg.service
```

nanoでファイルを開いたら以下のように記述

```bash
[Unit]
Description=Enable Wake on LAN
After=network-online.target
Wants=network-online.target

[Service]
Type=oneshot
ExecStartPre=/bin/sleep 30
ExecStart=/usr/sbin/ethtool -s enp3s0 wol g
RemainAfterExit=true

[Install]
WantedBy=multi-user.target
```

`After=network-online.target`はネット関係の起動が完了したあとに実行することを意味しているが、これでもうまくいかなかった。これはLANのインターフェイス名の書き換え前にコマンドが実行されてしまうためである(たしか)。ので`ExecStartPre=/bin/sleep 30`で30秒のsleepを入れている。

あとは有効化するだけ。

```bash
sudo systemctl daemon-reload
sudo systemctl enable wolg.service
sudo systemctl start wolg.service
```

### 自動化(暫定)

**上記の方法がうまくいかなかったので、現状はcrontab(root)を仕込んで5分毎に`ethtool -s enp3s0 wol g`を実行させている。**

```bash
sudo crontab -e -u root
```

以下を記入

```text
*/5 * * * * ethtool -s enp3s0 wol gs
```

スマートではないがとりあえずこれで確実に有効化できるようになったのでとりあえずはこれで。**サーバはy後いてほしいものが確実に動くことが大事なのでこれでいい**。あとこれくらいのコマンドなら何度実行しても負荷をかけたりバグったりすることはないだろうからええかというのもある。

起動後5分経過しないうちにPCがクラッシュor電源OFFとなると手動で立ち上げなければならないが、そのような自体になることは非常に稀であると期待している(流石に1分毎とかで実行はしたくなかった)。

### 起動を命令するPC(ラズパイ)

起動コマンド(マジックパケット)を送信する側のPCにwakeonlanコマンドをインストール

```bash
sudo apt -y install wakeonlan
```

これだけ。起動は`wakeonlan [起動したいPCのMACアドレス]`とすれば良い。ただ毎回MACアドレスを入力するのはめんどくさいのでスクリプト`~/wakeup.sh`を用意しておく。

```bash
#!/bin/bash
wakeonlan [起動したいPCのMACアドレス]
```

これを実行するには

```bash
./wakeup.sh
```

とすれば良い。起動したい時にこのスクリプトを実行すればいいので簡単。`crontab`で設定しておけば特定の時間にPCを起動させることができる。

- 参考: 
    - [systemdを用いたプログラムの自動起動](https://qiita.com/tkato/items/6a227e7c2c2bde19521c)
    - [CentOS 7起動時にプログラムを1回だけ自動実行する（RunOnceのように）](https://qiita.com/3244/items/b1f25705563da15879bd)
    - [ネットワークを利用するサービスの起動を遅延する](https://blog.bitmeister.jp/?p=5065)