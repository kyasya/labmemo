# Mailの自動送受信ソフトを作る(新)

前まで送受信一緒にしてたけど分けた。まずはGmail APIを使ってメールを取得するまでしようという話

## 受信側のプログラム
Outlookでなんとかしようと思ったけどusernameとpasswdが会ってるのにも関わらずloginができなかった。エラーでググるとMicrosoftが提供するOAuth2を使えとのことらしい、しかし大学のメールアカウントでは大学側が制限をかけているためかできなかったので、やり方を変えることにした。

現在私が所持しているメールアカウントは3つである。

1. gmail0: 高校時代に作ったメインアカントのgmail
1. outlook: 大学から提供されているMicrosoftアカウント
1. gmail1: 所属が変わったりしてメールが使用できない間に使用したり、お金(カード)など大事なメールなどを受信してOutlookに転送するために急遽作成したやつ

ここで、outlookからgmail1に届いたメールをgmai1から転送したものを除いて、すべて転送し、Gmail APIを用いて解析するという作戦だ。本当はOutlookを直接操作してタグ付けや自動仕分けをさせたかったが…今はいい案が思いつかないので妥協するしかない。

Gmail APIの設定(APIの有効化、OAuth使用の同意)とメールの送受信はここを見ればできる。
[Python を使い、Gmail API 経由で Gmail の送受信を行う](https://qiita.com/muuuuuwa/items/822c6cffedb9b3c27e21)

Mar.28.2023追記

OAuthの使用はテストモードにする(無料版だと外部設定しかしかできないのでテストユーザーという形で非公開運用する)。またテストユーザーに自分のgoogleアカウントを登録しないと、APIにアクセスしたときにアクセス拒否されるのできちんと設定しておくこと。

OAuthのIDと鍵はJsonでもダウンロードできるのでメモと同時にやっておくといいだろう。

### 受信側のプログラム

ということで受信プログラムを書いていく。いろいろネットで送受信のプログラムを見たがやはりオリジナルの改造がされているので難しい。のでやはり[公式ドキュメント](https://developers.google.com/gmail/api/quickstart/python?hl=ja)を頼るしかない。ここの説明とソースコードを見ると**APIを使用できるようになるまでの流れは**以下のような仕組みで動いているらしい。

1. まず前節で発行したOAuthの認証情報のjson(`credentials.json`に名前を変更しておく)を使ってgoogleに接続、認証情報のリクエストを送る
2. Googleサーバー側から操作側にAPI実行の許可を確認する画面が立ち上がる(ここは手動操作)
3. 認証が成功すると更新トークン(サンプルプログラムでは`token.json`)が発行される(次回からはこっちが使われる)
4. 更新トークンを使ってAPIに接続する

ということで初回のみ認証情報のjsonを使って更新トークンを発行し、更新トークンを使ってAPIに接続するという形になっている。実際サンプルプログラム`quickstart.py`を見ると、

1. 更新トークンの有無を確認
2. なければ認証情報のjsonを使って更新トークンを発行&保存
3. 更新トークンを使ってAPIを呼び出し

となっている。まずはこのサンプルを丸々パクって独自のクラスに処理段階ごとに分けて実装することにしよう。ちなみに最終的なプログラムの構想は以下のとおりである。

![Class](../image/memo/GMail.png){width="800"}

今回開発するのが`GMailCont`クラスで、最終的にはGoogle calendar APIを管理するクラス`GCalendarCont`クラスと組み合わせて、メール解析-->予定を抽出してcalendarに登録までできるようにするのが目標である。図右側は今回開発したクラスとjsonファイルのファイル構造である。サンプルプログラムと違ってjsonファイルはjsonディレクトリ下に置くことにしたのでディレクトリの指定の変更が必要である。

### まずはトークン認証から

まず、APIに関するpythonのライブラリはGoogleさんが用意してくれているのでpipでインスコ

```
python -m pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib
```

あとは`quickstart.py`をパクって実装。サンプルプログラムの流れ(前述)に従って、「更新トークン確認-->接続&認証と」、「実際のAPI呼び出し」部分に分けた。また両方で使う部分はクラスの関数として実装したほか、ファイルのパスの部分(認証json、更新トークンjson)のパスの部分も変数に置き換えて、`Set...(self, path)`関数で後から変更もできるようにした。(calendarでjsonを発行したときにかぶったりとしたときのため)

これらを踏まえて動作したプログラムは以下の通り

```py title="Mail.py"
#%%
from __future__ import print_function

import os.path

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
# pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib

class GMailCont:

    # If modifying these scopes, delete the file token.json.
    SCOPES = ['https://www.googleapis.com/auth/gmail.readonly']
    Creds = None

    OAuthToken = './json/credentials.json' # 認証トークンのPath
    UpdateToken = './json/token.json' # 更新トークンのpath

    def __init__(self) -> None:
        pass

    # jsonのファイルパス書き換え用関数
    def SetOAuthToken(self, path):
        self.OAuthToken = path
    def SetUpdateToken(self, paht):
        self.UpdateToken = paht

    # API使用のためのトークンの確認&接続
    def ConnectAndGetToken(self):
        # The file token.json stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.

        # 更新トークンあるかチェック&認証　
        if os.path.exists(self.UpdateToken): #ある時-->認証
            self.Creds = Credentials.from_authorized_user_file(self.UpdateToken, self.SCOPES)

        # If there are no (valid) credentials available, let the user log in.: ないとき
        if not self.Creds or not self.Creds.valid:

            if self.Creds and self.Creds.expired and self.Creds.refresh_token:
                self.Creds.refresh(Request())

            else:
                flow = InstalledAppFlow.from_client_secrets_file(self.OAuthToken, self.SCOPES)
                self.Creds = flow.run_local_server(port=0)

            # Save the credentials for the next run: 次回Runに向けて更新トークンを保存
            with open(self.UpdateToken, 'w') as token:
                token.write(self.Creds.to_json())

    def RunAPI(self):
            try:
                # Call the Gmail API
                service = build('gmail', 'v1', credentials=self.Creds)
                results = service.users().labels().list(userId='me').execute()
                labels = results.get('labels', [])

                if not labels:
                    print('No labels found.')
                    return
                print('Labels:')
                for label in labels:
                    print(label['name'])

            except HttpError as error:
                # TODO(developer) - Handle errors from gmail API.
                print(f'An error occurred: {error}')


a = GMailCont()
a.ConnectAndGetToken()
a.RunAPI()
```

最後の3行はデバックのためのものであり、最終的には消す部分。

関数一覧
- `ConnectAndGetToken(self)`: 更新トークンの有無のチェック->認証/更新トークンの発行
- `RunAPI(self)`: API実行今回はラベル(Gmailの左側にあるタブ)を表示しているらしい

結果
```
Labels:
CHAT
SENT
INBOX
IMPORTANT
TRASH
DRAFT
SPAM
CATEGORY_FORUMS
CATEGORY_UPDATES
CATEGORY_PERSONAL
CATEGORY_PROMOTIONS
CATEGORY_SOCIAL
STARRED
UNREAD
```

### 受信ボックスを除くところまで

ここからが本番。メールの一覧を取得して解析しなければならない。といってもメインのメールアドレスではないので解析までしてもあまり意味ないが、どちらにせよ中身がのぞけなければ意味がない。次は`RunAPI(self)`を書き換えていくことを目標にする。

まず[公式ドキュメント]のリファレンス欄をみると`users.messages.list`のメソッドを使えばいいらしい。([リファレンス](https://developers.google.com/gmail/api/reference/rest/v1/users.messages/list?hl=ja))つまり、リファレンスに合うようにRunAPIのresultの`... .labels().list(userId='me')...`のlabel部分を書き換える。書き換えた後残りの部分をいったんコメントアウトして、print文でresultを書き出してみると
```
{'messages': [{'id': '********', 'threadId': '*******'},...
```
というようにjson形式でメッセージのIDが出力されているのがわかるので、それをlistの時と同じように`get`関数を使って目的のメッセージIDを取り出す。[`get()`関数のリファレンスはこっち](https://developers.google.com/gmail/api/reference/rest/v1/users.messages/get?hl=ja)
ここまでまとめるとこんな感じ

```python
service = build('gmail', 'v1', credentials=self.Creds)
results = service.users().messages().list(userId='me').execute()
# meはユーザー認証をスキップできるワード
messages = results.get('messages') # メッセージID取得
# print(messages)

if not messages:
	print('No messages found.')
	return
print('Messages:')
for i_message in messages:
	# print(i_message)
	# 本文とか取得
	this_result = service.users().messages().get(
		userId='me', id=i_message['id']).execute()
	print(this_result)
```

で取得された例がこんな感じ。これもjson形式になっている。
```
Messages:
{'id': '********', 'threadId': '***', 'labelIds': ['INBOX'], 'snippet': '本文', 'payload': {'partId': '',....
```
ということで本文とヘッダ(送信元、件名)を取得して、hdfに保存しよう。そのための解析関数`AnalysisMessage`を追加する。返り値は辞書型とした。