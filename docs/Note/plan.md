# 補助ソフトの構想
イントロで書いたやつの続き

## 整備したいソフト
- [x] メール送信ソフト: 添付ファイル等も貼れたら活用幅が広がりそう
- [ ] メール解析: メールの内容を解析し、重要予定をピックアップ、予定表に登録する
- [ ] 論文の更新チェッカー:関連する論文をピックアップ。査読・出版状況を自動チェックし更新等があればメールで通知
- [ ] 著者データチェッカー: 共同研究者のデータベースをチェック。住所変更などがあれば更新してメール送信
- [ ] AllinOneコンソール: 上記のソフトを一元管理するためのGUIプログラム

## 参考になりそうなサイト
- [Googleカレンダー：Pythonから予定作成・読み込み](https://www.yutaka-note.com/entry/2020/02/06/080631)
- [PythonでGoogleカレンダーAPIを使用する方法](https://maasaablog.com/development/backend/python/4744/)
- [pythonでGoogleカレンダーに予定を追加する(準備編)](https://nikawa-niwaka.net/entry/1501)

論文のチェックはスクレイピングも考えたがAPIがあるかなと思ってググったら出てきた。同じことやってる人もいるしできそう。
- [arXivAPI](https://www.google.com/search?q=arXiv+api&rlz=1C1CHBD_jaJP977JP977&oq=arXiv+api&aqs=chrome..69i57j0i512j0i30l6j0i8i30l2.5457j0j7&sourceid=chrome&ie=UTF-8)
