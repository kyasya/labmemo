# 論文&実績一覧

中の人が書いた論文の一覧です。

# 出版済論文 (Published paper)


!!! Tip "第37回 放射線検出器とその応用 研究会"
	- **Notification date**: 2024. Feb. 7.
    - **Title (English)**: PICOLON DARK MATTER SEARCH ～STATUS OF BACKGROUND ANALYSIS FOR HIGH-PURITY NaI(Tl)～
    - **Title (Japanese)**:PICOLON宇宙暗黒物質探索 ～高純度NaI(Tl)結晶のバックグラウンド解析の現状～
	- **Publisher**: K. Kotera. et. al. (I'm first author).
	- **Affiliation**: Graduate School of Advanced Technology and Science, Tokushima University, Japan.
	- **Report No.**: KEK Proceedings 2023-1.
	- **Language**: Japanese.
	- **Title**: Proceedings of the 37th Workshop on Radiation Detectors and Their Use.
	- **Edited by**: Sanami, T. et. al. (KEK, Tsukuba)(Eds.).
	- **Cite (long)**: K. Kotera et. al., Proceedings of the 37th Workshop on Radiation Detectors and Their Use, KEK, 141-148, In japanese, (2023).
	- **Cite (short)**: K. Kotera et. al., KEK Proceedings 2023-1, 141-148, In japanese, (2023).
	<!-- - KEK: High Energy Accelerator Research Organization -->

	- URL(HP): [https://www.i-repository.net/il/meta_pub/G0000128Lib_202325001](https://www.i-repository.net/il/meta_pub/G0000128Lib_202325001)
	- URL(PDF): [https://lib-extopc.kek.jp/preprints/PDF/2023/2325/2325001.pdf](https://lib-extopc.kek.jp/preprints/PDF/2023/2325/2325001.pdf)

	**Abstract (In English only):**
	  
	PICOLON (Pure Inorganic Crystal Observatory for LOw-energy Neut(ra)lino) is an experiment to search for the dark matter using high-purity NaI(Tl) crystals. 
	Weakly interacting massive particles (WIMPs) have been proposed as one of the candidates for the dark matter. The DAMA/LIBRA group search for the dark matter using a 250 kg high-purity NaI(Tl) detector, and they have been reporting the annual modulation at an astounding 12$\sigma$ confidence level. However, their results have not been confirmed by other groups using similar NaI(Tl) crystals. The ANAIS and SABRE groups have ruled out the possibility of the annual modulation using NaI(Tl). However, the COSINE group reported the annual modulation using the DAMA/LIBRA like method of data analysis.
	
	In PICOLON, Ingot #85 (produced in 2020) has succeeded in achieving purity and has been confirmed to be as pure as DAMA/LIBRA crystals.
	In this report, we report the background in the Ingot #94 crystal, which was produced using the same purification method as the Ingot #85 crystal. We compared the background in the crystals by extraction of $\alpha$-ray events. The results showed that the background concentration in the crystals were $^{232}$Th=4.6$\pm$1.2 $\mu$Bq, $^{226}$Ra=8.7$\pm$1.5 $\mu$Bq, and $^{210}$Po=28$\pm$5 $\mu$Bq. The background was comparable to that of the high-purity crystals of the DAMA/LIBRA group. We confirm the reproducibility of the purification method used to produce the Ingot #85 crystals.

# 発表等


!!! Tip "発表実績(2024)"
	- Jan. 17 - Jan. 19: FPUR研究会 (福井大学, 日本)
	- Jan. 22 - Jan. 24: 検出器とその応用 (KEK, 筑波, 日本)
	- Feb. 6 - Feb. 7: [極低放射能研究会 (横浜国立大学, 日本)](https://www.lowbg.org/ugap/ws/lb2023/index.html)
	- Mar. 4 - Mar. 5 : [UGAP2024 地下宇宙国際シンポジウム (仙台,日本)](https://www.lowbg.org/ugap/ws/am2023/index.html?LangP=EN&InputContents=Prog)
	- Mar. 6 - Mar. 7 : [地下宇宙若手研究会 (仙台, 日本)](https://www.lowbg.org/ugap/ws/yr2023/index.html)
	- Jul. 7 : [UGRP領域研究会 (阪大, 大阪, 日本)](https://www.lowbg.org/ugrp/workshop/am2024/index.html)
	- Sep. 5 - Sep. 7 : [DMNet 国際シンポジウム (太田, 韓国, **招待講演**)](https://indico.ibs.re.kr/event/664/)
	- Sep. 16 - Sep. 19 : [日本物理学会 79回年次大会(札幌, 北大)](https://onsite.gakkai-web.net/jps/jps_search/2024au/index.html)
	- Oct. 15 - Oct. 17 : [SMART研究会 2024(伊東, 伊東温泉)](https://smart20170.wixsite.com/smart2024)
	- Jun. 27 - Jun 29 : [第39回 研究会「放射線検出器とその応用」(筑波, KEK)](https://conference-indico.kek.jp/event/304/)

!!! Tip "発表実績(2023)"
	- 7.30-8.3: 国際会議 ICRC2023 (名古屋大学, 日本).
	- 8.7-8.9: シンチレータ研究会 SMART2023 (下呂, 日本).
	- 8.28-9.1 (CEST): 国際会議 TAUP2023 (Universtät Wien, Östareich).
	- 9.17-9.20: 物理学会 (東北大学, 日本).

# 受賞歴・職歴

!!! Tip "徳島大学 非常勤講師"
	徳島大学 非常勤講師 (2024-)

!!! Tip "徳島大学 TA"
	徳島大学 TA (2020-)

!!! Tip "康楽賞"
	康楽賞(奨学生の部) 受賞
	URL: [令和4年度康楽賞受賞について](https://www.tokushima-u.ac.jp/docs/46106.html)