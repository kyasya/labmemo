# リンク集 <!-- omit in toc -->

- [物理](#物理)
  - [レビューと会議(コミュニティ)](#レビューと会議コミュニティ)
  - [データベースなど](#データベースなど)
  - [お役立ち](#お役立ち)
  - [Geant4 \& ROOT](#geant4--root)
- [Linux](#linux)
- [C/C++](#cc)
  - [基礎](#基礎)
  - [リファレンス](#リファレンス)
  - [便利](#便利)
  - [GUI](#gui)
- [Python](#python)
  - [グラフ描画](#グラフ描画)
  - [通信(シリアル、TCP)](#通信シリアルtcp)
  - [GUI開発](#gui開発)
  - [API](#api)
  - [お役立ち、面白いやつ](#お役立ち面白いやつ)
  - [ライブラリの公開、import/include](#ライブラリの公開importinclude)
- [その他プログラミング](#その他プログラミング)
  - [Rust](#rust)
- [趣味](#趣味)
- [書物: Git、Markdown、LaTeX](#書物-gitmarkdownlatex)
  - [静的サイトジェネレーター](#静的サイトジェネレーター)
    - [mkdocs マニュアルなど](#mkdocs-マニュアルなど)
    - [Markdown記法](#markdown記法)
    - [その他](#その他)
  - [LaTeX](#latex)
    - [見栄え・自作ライブラリ](#見栄え自作ライブラリ)
    - [描画(Tikz)と便利ツール](#描画tikzと便利ツール)
    - [スライドもLaTeXで](#スライドもlatexで)
    - [LaTeXの代替(typst)](#latexの代替typst)
- [お役立ち](#お役立ち-1)

メモページに使おうと思っていたら、完全にリンク集になってしまったページ

## 物理

上側は会議とレビュー(研究や分野の紹介)、残りはデータベース

### レビューと会議(コミュニティ)

- [Google Scholar](https://scholar.google.co.jp/schhp?hl=ja&as_sdt=0,5)
- [高エネルギー物理学研究者会議(JAHEP)](https://www.jahep.org/index.html)

### データベースなど

- [Reviews, Tables & Plots](https://pdg.lbl.gov/2023/reviews/contents_sports.html)
- [X-Ray Mass Attenuation Coefficients (NIST)](https://physics.nist.gov/PhysRefData/XrayMassCoef/tab3.html)
- [National Nuclear Data Center](https://www.nndc.bnl.gov/)
- [Neutrino fluxなど](http://www.sns.ias.edu/~jnb/)
- [放射線取扱主任者試験 (第1種、第2種)](https://www.nustec.or.jp/syunin/syunin05.html)
- [arXiv](https://arxiv.org/)
- [CAEN](https://www.caen.it/sections/firmwaresoftware/)
- [observation of a neutrino burst from the supernova sn1987a](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.58.1490): カミオカンデ
- [The Experimental Foundations of Particle Physics](http://hitoshi.berkeley.edu/129a/cahn-goldhaber/)

### お役立ち

- [Richard Feynman - Science Videos: ファイマンさんのビデオ？！ご冗談でしょう？](http://www.vega.org.uk/video/subseries/8)
- [浅井研究室（物理教室・素粒子物理国際研究センター兼任）](https://www.icepp.s.u-tokyo.ac.jp/~asai/)
- [個人向けデジタル化資料送信サービス](https://www.ndl.go.jp/jp/use/digital_transmission/individuals_index.html): 絶版の本がほしいときに使える
    - [有益なTwitter情報](https://twitter.com/juvenile_crimes/status/1519619349468057601)
- [unlimited:waifu2x: 画像の高画質化に使う](https://unlimited.waifu2x.net/)
- [Discord STREAMKIT ~OVERLAY FOR OBS & XSPLIT~](https://streamkit.discord.com/overlay): 誰が喋ってるかも記録したいときに。配信で使うのが正規の使い方。

### Geant4 & ROOT

- Geantについては専用のページを立ち上げる予定

- [ROOT(公式)](https://root.cern/)
- [ROOT(Manual)](https://root.cern/primer/)
- [ROOT(Reference Guide)](https://root.cern/doc/v628/)
- [ROOT](https://www.quark.kj.yamagata-u.ac.jp/~miyachi/ROOT/index.html)
- [RROOT/ROOT入門](https://azusa.shinshu-u.ac.jp/~hasegawa/wiki/index.php?ROOT/ROOT%E5%85%A5%E9%96%80)
- [Tips & tricks for software used at COMPASS(chap.5: ROOT)](https://physics.mff.cuni.cz/kfnt/cern/softintro/?chapter=5)
- [見栄えを整える](https://www.rcnp.osaka-u.ac.jp/~kohda/root/looks.html)
- [ROOT COMMAND MEMO](http://be.nucl.ap.titech.ac.jp/~togano/root.html)
- [memo/root/tree](http://be.nucl.ap.titech.ac.jp/~kondo/wiki/index.php?memo/root/tree)
- [KamonoWiki](https://www-he.scphys.kyoto-u.ac.jp/member/n.kamo/wiki/doku.php?id=study:software:root)
- [並列計算の参考code](https://root.cern/doc/v612/group__tutorial__multicore.html)

## Linux

- [tcshメモ書き](https://qiita.com/earth06/items/956fc337ce6cdcade646)
- [【bash入門】bashシェルスクリプトの書き方](https://tech-blog.rakus.co.jp/entry/20210525/shellscript)
- [Bash Scriptの作法](https://qiita.com/autotaker1984/items/bc758fcf368c1a167353)
- [デュアルブートシステム用の共有ストレージドライブを作成する方法](https://jp.tipsandtricks.tech/%E3%83%87%E3%83%A5%E3%82%A2%E3%83%AB%E3%83%96%E3%83%BC%E3%83%88%E3%82%B7%E3%82%B9%E3%83%86%E3%83%A0%E7%94%A8%E3%81%AE%E5%85%B1%E6%9C%89%E3%82%B9%E3%83%88%E3%83%AC%E3%83%BC%E3%82%B8%E3%83%89%E3%83%A9%E3%82%A4%E3%83%96%E3%82%92%E4%BD%9C%E6%88%90%E3%81%99%E3%82%8B%E6%96%B9%E6%B3%95)
- [Windows 11のWSL2環境でUbuntuデスクトップ環境を構築](https://wisenetwork.net/archives/4739)
- crontab: [システム起動時に特定のコマンドを実行するには](https://atmarkit.itmedia.co.jp/flinux/rensai/linuxtips/a029crontabstartup.html)
- [mailコマンドでメールの閲覧や送信を行う](https://monologu.com/mail-command/)
- [[mailコマンド]Linuxからメールを送る](https://qiita.com/shuntaro_tamura/items/40a7d9b4400f31ec0923)
- [USBシリアル接続時に自動で書込権限を付与する（ubuntu,/dev/ttyACM0）](https://www.robotech-note.com/entry/2018/07/23/ubuntu%E3%81%A7USB%E3%82%B7%E3%83%AA%E3%82%A2%E3%83%AB%E3%83%87%E3%83%90%E3%82%A4%E3%82%B9%EF%BC%88/dev/ttyACM0%E7%AD%89%EF%BC%89%E6%8E%A5%E7%B6%9A%E6%99%82%E3%81%AB%E8%87%AA%E5%8B%95%E3%81%A7%E6%9B%B8)
- [Ubuntu-22.04のウィンドウマネージャ](https://zenn.dev/waruby/articles/513f3edb808d4c)
- [Ubuntu で OneDrive を使おうか](https://note.com/rich_mango981/n/neaca93c05cfe)
- Linuxじゃないけどいったんここに
    - [はじめての VS Code 拡張機能開発](https://zenn.dev/hiro256ex/articles/20230625_getstartedvscodeextension)
    - [VSCodeの拡張機能を作る手順](https://zenn.dev/daifukuninja/articles/13a35a8bb3a4a1)
    - [VSCode拡張機能を作ってみる2 - 入力補完](https://qiita.com/qvtec/items/31d19dd8b86fcc19465a)
- [内蔵HDDを/etc/fstabを使って自動マウント](https://heppoko-room.net/archives/1878): 新品HDDをマウントする方法(Linux)
- [ubuntuで内蔵HDDを自動マウントする](https://qiita.com/k-Mata/items/276741f7fbfa02e9b7e9)
- [「Linux」でドライブの自動マウントを有効にするには](https://japan.zdnet.com/article/35204011/)
- [mountエラー「wrong fs type, bad option, bad superblock on /dev/sdb」](https://ex1.m-yabe.com/archives/3457)
- [Linuxの各種スペック確認](https://qiita.com/speedstar18fct/items/dc8f2b97f0be659de65f)
- [【サルでも分かる】OpenVPN入門](https://www.mtioutput.com/entry/2018/10/30/090000)
- [LinuxでUSB機器を配線したまま接続のON/OFFを行う](https://qiita.com/m-m-n/items/55e70f7981f624f40437)
‐ [OpenVPN 構築: 自宅外からVPN を使用して自宅内のマシンへのアクセスと、公衆Wifi を使用したときの全パケット暗号化](https://qiita.com/TsutomuNakamura/items/1e58fac6ad21c2226ba4)

## C/C++

### 基礎

- [ロベールのＣ＋＋教室](http://www7b.biglobe.ne.jp/~robe/cpphtml/)
- [ゼロから学ぶ C++](https://rinatz.github.io/cpp-book/)
- [ビット演算](https://monozukuri-c.com/langc-bitoperation/)
- [ビット演算](https://serip39.hatenablog.com/entry/2022/03/31/120000)
- [関数から複数の戻り値を返す (std::tuple)](https://qiita.com/dfukunaga/items/e2375369d5b280e4a939)
- [浮動小数点数型と誤差](https://www.cc.kyoto-su.ac.jp/~yamada/programming/float.html)
- [【C++】クラスの前方宣言まとめ](https://qiita.com/shuheilocale/items/31923586ab495217742a): クラス内に別クラスの変数があるときに使う。
- [簡潔なコードを書くために](https://qiita.com/nomikura/items/f6f6f6c1507672df39a1)
- [C言語 goto文【使うケースと使ってはダメなケースを解説】](https://monozukuri-c.com/langc-funclist-goto/)
    - `goto`文は凶悪な構文だが、C言語には例外処理がなく使わざる得ない時があるので。
- [【C言語】printf で 左詰め 右詰め ゼロ埋めを行う方法と豆知識](https://marycore.jp/prog/c-lang/left-right-zero-padding/):フォーマット文字列の話


### リファレンス

- [std::array (cpprefjp - C++日本語リファレンス)](https://cpprefjp.github.io/reference/array.html)
- [std::vector (cpprefjp - C++日本語リファレンス)](https://cpprefjp.github.io/reference/vector/vector.html)
- [std::map (cpprefjp - C++日本語リファレンス)](https://cpprefjp.github.io/reference/map/map.html)
- [std::tuple (cpprefjp - C++日本語リファレンス)](https://cpprefjp.github.io/reference/tuple/tuple.html)

### 便利

- [C++並列処理: openmp, thread, Eigen](https://lilaboc.work/archives/23793422.html)
- [C言語からgnuplotを操作する](http://www.yamamo10.jp/yamamoto/lecture/2007/5E_comp_app/gnuplot/html/node4.html)
- [boost::spirit::qiを使って構文解析する](https://www.pandanoir.info/entry/2017/12/21/190000)
- [imgui](https://github.com/ocornut/imgui):プラットフォームに依存しないC++ GUI Library。pythonを使いたくないときに使える？
    - [紹介元のサイト](https://qiita.com/shimacpyon/items/e5d2a4e2019273345c37)

### GUI

- [imgui](https://github.com/ocornut/imgui?tab=readme-ov-file#how-it-works)
- [imgui/docs/EXAMPLES.md](https://github.com/ocornut/imgui/blob/master/docs/EXAMPLES.md)
- [implot](https://github.com/epezent/implot?tab=readme-ov-file)


## Python

雑なプログラムなほど楽にかける。逆に言えば「ちゃんとした」ものを作るのはめっちゃ大変。最適化とか最適化とか最適化とか。

### グラフ描画

- [ROSmsgをmatplotlibでリアルタイム描画【rospy】](https://nayutari.com/python-socket)
- [matplotlibで矢印を描画したい! annotateを使おう!](https://qiita.com/haru1843/items/e85fe45163cd9763023c)
- [matplotlib で指定可能なマーカーの名前](https://pythondatascience.plavox.info/matplotlib/%E3%83%9E%E3%83%BC%E3%82%AB%E3%83%BC%E3%81%AE%E5%90%8D%E5%89%8D)
- [矢印プロットの作図【ゼロつく1のノート(Python)】](https://www.anarchive-beta.com/entry/2021/08/08/235829)
- [Python による放射線データの解析](https://hackmd.io/@tenoto/Skl_wArqD)
- [GridSpec](https://yyousuke.github.io/matplotlib/gridspec.html)

### 通信(シリアル、TCP)

- [socket --- 低水準ネットワークインターフェース(リファレンス)](https://docs.python.org/ja/3/library/socket.html)
- [Pythonでシリアル通信！Arduinoに文字列を送って、Arduinoから文字列を受け取る！](https://greenhornprofessional.hatenablog.com/entry/2020/09/13/223155)
- [PythonソケットによるTCP通信入門](https://nayutari.com/python-socket)
- [Pythonでソケット通信を実装しメッセージの送受信を行う](https://tech-blog.s-yoshiki.com/entry/246)
- [ソケット通信サンプル　〜プロセス間通信入門#1〜](https://www.mathkuro.com/python/inet-socket-sample/)
- [Python で独立したプロセス間の通信をやってみる](https://qiita.com/hypertombo/items/862060cd2f4bf6b7974d)
- [LinuxとC/C++によるソケット通信サンプル](https://www.koikikukan.com/archives/2017/10/31-000300.php)
- [Pythonとプロセス間通信！](https://jp-seemore.com/iot/python/10572/)
- [Pythonでソケット通信を行う方法-](https://gr-st-dev.tistory.com/2587)
- [OpenCVデータをC++とPython間で交換する方法](https://blog.futurestandard.jp/entry/2017/03/02/143449)
- [ソケット通信でc++からpythonへ、pythonからc++へデータを送る](http://blog.livedoor.jp/tmako123-programming/archives/51046977.html)
- [Pythonでのソケット通信（TCP/UDP）方法](https://niyanmemo.com/304/2/)
- [C++とPython間の通信構造体](https://libra23.hatenablog.com/entry/2021/11/14/170556)
- [C言語、Pythonによるソケット通信](https://qiita.com/fygar256/items/b96aebbd79b4cbf19c2f)

### GUI開発

- [PySimpleGUI example (trinket)](https://pysimplegui.trinket.io/demo-programs#/demo-programs/all-elements-simple-view)
- [【matplotlib】大量に画像を出力した際に発生したメモリ不足により落ちる現象への対処[Python]](https://3pysci.com/matplotlib_memoryleak-1/)
- [Pythonのthreading使ったらプログラムが止まらなかったパターンがある](https://qiita.com/KjumanEnobikto/items/ae9458d0be283db1887b)
- [plt.close() だけではメモリが解放されない場合がある](https://qiita.com/Masahiro_T/items/bdd0482a8efd84cdd270)

### API

- [Google Workspace API](https://console.cloud.google.com/apis/library/browse?filter=category:gsuite&hl=ja): googleにはAPIの一覧。自動化か楽したいときに使う
- [【RPA】Python×受信メール解析｜Gmail・IMAP認証・ヘッダー及び本文分析による業務自動化や迷惑メール対策](https://di-acc2.com/system/rpa/3031/)
- [deepL API](https://support.deepl.com/hc/ja/articles/360020691179-API%E6%8A%80%E8%A1%93%E8%B3%87%E6%96%99)
    - [main](https://developers.deepl.com/docs/api-reference/translate)、[使い方](https://qiita.com/m-hayashi/items/e2acc640fb436d09f128)


### お役立ち、面白いやつ

- [NuitkaでPythonをexe化しよう](https://qiita.com/yulily/items/b97dc34615eac2be7157)
- [imgsim】画像の類似度をPythonで手早く測りたい](https://qiita.com/kagami_t/items/a1cae07c9565ce501ced)
- [Raspberry Piでデータの可視化をしてみる[Grafana]](https://qiita.com/ebiflyyyyyyyy/items/dee5122952e1c0f0bc27#%E3%83%80%E3%83%83%E3%82%B7%E3%83%A5%E3%83%9C%E3%83%BC%E3%83%89%E3%81%AE%E4%BD%9C%E6%88%90)
- [クラスタ計算機](http://www2.yukawa.kyoto-u.ac.jp/~koudai.sugimoto/dokuwiki/doku.php?id=%E8%87%AA%E4%BD%9C%E3%82%AF%E3%83%A9%E3%82%B9%E3%82%BF%E8%A8%88%E7%AE%97%E6%A9%9F)
- [バイナリファイルを読み書きする](https://izadori.net/python-binaryfile/)
- [バイナリデータを入出力したい (struct)](https://ohke.hateblo.jp/entry/2020/02/22/230000)
- [Sambaサーバ（NAS）構築方法](https://e-topics.net/raspi-samba/)
- [ラズパイ４　撮影写真をブラウザで見る](https://kats-eye.net/info/2020/05/20/picture-upload2/)
- [タイムゾーン呪いの書 (知識編)](https://zenn.dev/dmikurube/articles/curse-of-timezones-common-ja)
- [Pythonの日付処理とTimeZone](https://nekoya.github.io/blog/2013/06/21/python-datetime/)
- [pythonでC++(std::vector)を呼び出す](https://www.letsopt.com/entry/2020/08/07/014629)
    - C++を使った最適化(高速化)の話
- [【初心者向け】Pythonで自動翻訳【Googletrans】](https://invisiblepotato.com/python11/)
- [PyMuPDFへようこそ](https://pymupdf.readthedocs.io/ja/latest/)
- [Nougat: Neural Optical Understanding for Academic Documents](https://facebookresearch.github.io/nougat/):
	 OCRを利用したPDF変換。**数式もLaTeXへ変換してくれる**。がAIなので**GPU必須**。([解説](https://www.12-technology.com/2023/09/nougat-ocrarxiv.html), [GitHub](https://github.com/facebookresearch/nougat#cli))
- [sentence-transformers 2.7.0](https://pypi.org/project/sentence-transformers/)
- [PythonのException(例外処理)　tracebackをファイル出力する方法](https://allmaintenance.jp/webdirection/programing/python/python%E3%81%AEexception%E4%BE%8B%E5%A4%96%E5%87%A6%E7%90%86%E3%80%80traceback%E3%82%92%E3%83%95%E3%82%A1%E3%82%A4%E3%83%AB%E5%87%BA%E5%8A%9B%E3%81%99%E3%82%8B%E6%96%B9%E6%B3%95/)
- [traceback --- スタックトレースの表示または取得](https://docs.python.org/ja/3/library/traceback.html)
- [Python concurrent.futureを用いた並列処理のさせ方メモ](https://note.com/mega_gorilla/n/n3fe706a9a950)
- [Pythonの構文解析ライブラリLarkを使って遊んでみました](https://lab.tricorn.co.jp/katsura/6268)
- [cppyy: Automatic Python-C++ bindings](https://cppyy.readthedocs.io/en/latest/)
- [poxy 0.16.0](https://pypi.org/project/poxy/)
- [Exhale](https://exhale.readthedocs.io/en/latest/)
- コンフィグファイルの読み込み
    - [TOML v1.0.0](https://toml.io/ja/v1.0.0)
    - [tomlのパーサを作った](https://in-neuro.hatenablog.com/entry/2017/05/19/153318): C++ (>C++11)
    - [TOML++ TOML FOR C++](https://marzer.github.io/tomlplusplus/): C++ (C++>17)

### ライブラリの公開、import/include

- [Pythonで自分だけのクソライブラリを作る方法](https://zenn.dev/karaage0703/articles/db8c663640c68b): python
- [Python __init__.pyの書き方](https://qiita.com/FN_Programming/items/2dcabc93365a62397afe)
- [Python 3でのファイルのimportのしかたまとめ](https://qiita.com/karadaharu/items/37403e6e82ae4417d1b3)
- [Shields.io](https://shields.io/badges): githubのREADMEについてるバッジ生成するやつ
- [【PyPI 】Pythonの自作ライブラリをpipに公開する方法](https://qiita.com/c60evaporator/items/e1ecccab07a607487dcf)
- [MkDocsによるドキュメント作成【Python】](https://liquids.dev/articles/bb24b6e9-eb6f-4ebf-a6c9-3b583dce5c00)
- [docstringのstyle3種の例](https://qiita.com/yokoc1322/items/ebf25c9cb779ff5ebc9c): Googleスタイルが好き(隙自語)
- [MkDocsを使って、簡単にドキュメント公開♪](https://qiita.com/yagizo/items/fef66728f97b866a3bee)
- [doxybook2](https://github.com/mkdocstrings/mkdocstrings/issues/97): mkdocsでC++のドキュメントも生成したかったから...探してたら見つけたやつ
- [Documenting C++ Code with Sphinx](https://medium.com/@aytackahveci93/documenting-c-code-with-sphinx-d6315b338615)

## その他プログラミング

### Rust

- [rust狂信者のための書き散らしグラフ生成のススメ](https://zenn.dev/waarrk/articles/175f19e37c2169)
- [Crate plotters](https://docs.rs/plotters/latest/plotters/)

## 趣味

- [Arduino Microテクニカルノート](https://cammy.co.jp/technical/arduino-micro/)
- [マウスカーソルづくりのススメ](https://note.com/onion_189/n/n1cc2f49ab9aa)
- [Raspberry Pi編 第４回USBカメラをつなげてみる](https://www.sys-link.jp/it/electronic-kit/raspberrypi/raspberrypi-004/)
- かっこいいマスコンを作ってる人: [でんちょく電機](https://smee-tec.studio.site/)
- [JLCPCBﾆｷ](https://jlcpcb.com/JPV?from=VGPJP&gad_source=1&gclid=Cj0KCQjwwMqvBhCtARIsAIXsZpY2IISw9dzbnYN4kcMm2Hs3N6wYhmGiWbzoGuUvUcgaJhnk-R8HJ9MaAqQvEALw_wcB)
- 強化版ラズパイ。つよい、はやい、高い。: [Orange Pi 5](http://www.orangepi.org/html/hardWare/computerAndMicrocontrollers/details/Orange-Pi-5.html)
- [はんこ](https://www.minna-hanko.jp/monochromator)
- 競馬
    - [競走馬の強さの数値化に挑戦する](https://qiita.com/daifukusan/items/8d3913755704554a71c9)
    - [複勝とワイドの中間オッズの逆算](https://qiita.com/taniyam/items/99091939b5f304fe3c37)
    - [他多数](https://qiita.com/tags/%e7%ab%b6%e9%a6%ac)

## 書物: Git、Markdown、LaTeX

### 静的サイトジェネレーター

#### mkdocs マニュアルなど

- [GitLab日本語ドキュメント](https://gitlab-docs.creationline.com/ee/user/markdown.html)
- [GitLab Flavored Markdown (GLFM) (FREE)](https://gitlab.com/gitlab-org/gitlab/blob/master/doc/user/markdown.md#task-lists)
- [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/reference/)
- [MkDocs拡張機能](https://kurotori4423.github.io/KurotoriMkDoc/MkDocs%E8%A7%A3%E8%AA%AC/Extension/)
- [mkdocs.yml 設定](https://kojion.github.io/chess/mkdocs/004/)
- [MkDocs(by ふうせん🎈 FU-SEN さん)](https://balloon-jp.vercel.app/mkdocs/)
- [tick-taku's diaryObsidian と MkDocs と GitHubActions と Netlify で君だけの最強日記環境を作りあげろ！](https://tick-taku.com/blog/obsidian_mkdocs_githubactions_netlify_snk/)
- [mkdocs-meta-descriptions-plugin](https://pypi.org/project/mkdocs-meta-descriptions-plugin/)
- [mkdocs-swagger-ui-tag 0.6.9](https://pypi.org/project/mkdocs-swagger-ui-tag/)
- [【Font Awesome】初心者でもすぐ使える！簡単Webアイコン入門](https://blog.codecamp.jp/font-awesome-icon)
- [mkdocs-table-reader-plugin](https://timvink.github.io/mkdocs-table-reader-plugin/): CSVが読み込めるようになる
- OGP/Twitter card等
    - [Twitter Cards](https://kojion.github.io/chess/mkdocs/006/)
    - [Google による robots.txt の指定の解釈](https://developers.google.com/search/docs/crawling-indexing/robots/robots_txt?hl=ja)
    - [ツイートをカードで最適化する (公式)](https://developer.twitter.com/ja/docs/tweets/optimize-with-cards/guides/getting-started)
    - [サイトのOGP画像を自動生成する](https://zenn.dev/panda_program/articles/generate-og-image)
    - [【記事タイトル入りog:image自動生成】(ry)](https://docs.sakai-sc.co.jp/article/programing/og-image-generate.html)
    - [今まで手動で作成していたOGP用画像をPythonの画像処理ライブラリPillowを使って生成する](https://zenn.dev/nekocodex/scraps/cbf094d5d07bd867e475)
    - [PythonでPillowを使ってOGP画像を作ろう](https://zenn.dev/makiart/articles/78d53694e70105)
    - 多分使っても意味なさそう: [Card validator](https://cards-dev.twitter.com/validator)

#### Markdown記法

- [Markdown記法 チートシート](https://qiita.com/Qiita/items/c686397e4a0f4f11683d)
- [Qiitaのマークダウンで色をつける方法[140色]](https://qiita.com/twipg/items/d8043cd4681a2780c160)

#### その他

- [デジタル庁のサイトやばすぎるwww](https://qiita.com/mu_tomoya/items/f78f1fad3a8b57ac7dc3)
- [VNC自動化: Connections: blacklisted: の対処方法](https://qiita.com/tukiyo3/items/cbee3c1e2150c4f0df87)

### LaTeX

- [LaTeXにおける正しい論文の書き方](https://qiita.com/birdwatcher/items/5ec42b35d84d3ee2ffbb)

#### 見栄え・自作ライブラリ

- [LaTeXでシナリオ同人誌が出ました](https://note.com/komuda/n/n6ba037669fdf)
- [titlesecパッケージで見出しの書式を変える](https://tobetakoyaki.hatenablog.com/entry/2021/04/10/221137)
- [LaTeXでパッケージ(スタイルファイル)を自作する](https://qiita.com/Shunt/items/33c72706afaedce8bb9f)

#### 描画(Tikz)と便利ツール

- [TikZサンプル集～座標上の図形編～](https://pictblog.com/tikz-sample-figureandcoord)
- [TikZ: BeamerのためのTikZ](https://tasusu.github.io/tikz.html)
- [TikZ & PGF Manual for Version 3.1.10](https://ftp.kddilabs.jp/CTAN/graphics/pgf/base/doc/pgfmanual.pdf)
- [フォーム変換ツール](https://tableconvert.com/ja/csv-to-latex)
      - ExcelやcsvのデータをLatexyaMarkdownほか複数のフォーマットに変換してくれる。めっちゃ便利

#### スライドもLaTeXで

- [Beamer (Metropolis) でスライドを作る](https://zenn.dev/shosuke_13/articles/7ffdf45cb5da6c)
- [mholson/sthlmNordBeamerTheme](https://github.com/mholson/sthlmNordBeamerTheme)

#### LaTeXの代替(typst)

まだ完璧ではないらしい。(そりゃ新興だからパッケージ関係とかはまだ貧弱だよな...)
後数年くらいすればめっちゃ便利な代替組版ソフトになりそう。

- [Typst Documentation](https://typst.app/docs/reference/syntax/)
- [Typst： いい感じのLaTeXの代替](http://www-het.phys.sci.osaka-u.ac.jp/~yamaguch/j/typst.html)

## お役立ち

- [WebPlotDigitizer](https://automeris.io/WebPlotDigitizer/)
- [科研費.com](https://xn--w8yz0bc56a.com/)