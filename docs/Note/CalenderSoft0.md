# Google calendar APIを使って予定の自動追加

メールのメッセージを解析して予定を追加するプログラムを書くためにさらにcalendar APIを使う部分を策に作っておく

## APIの使い方
やはり[公式ドキュメント](https://developers.google.com/calendar/api/quickstart/python?hl=ja)が役に立つ。サンプルプログラムを見たところ更新トークンの部分はそのまま使いまわせそうなのでMail.pyをコピペ、Calendar.pyにリネームして`Mail`classを以下のように書き換えた。

- ファイル名の変更`Mail.py`-->`Calendar.py`
- クラス名の変更:`GMailCont`-->`CalenderCont`
- 変数`SCOPES`の値を`['https://www.googleapis.com/auth/calendar.readonly']`に変更
- 更新トークンの名前を変更(メールの更新トークンとの被りを防止):`UpdateToken = './json/token_calender.json'`
- `RunAPI()`関数を変更

ということで書き上げたのがこちら(**後で大幅に変更することになる**)

```py
#%%
from __future__ import print_function

import os.path
import datetime

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

class CalendarCont:

    # If modifying these scopes, delete the file token.json.
    SCOPES = ['https://www.googleapis.com/auth/calendar.readonly']
    Creds = None

    OAuthToken = './json/credentials.json' # 認証トークンのPath
    UpdateToken = './json/token_calendar.json' # 更新トークンのpath

    # -- ここから関数
    # コンストラクタ
    def __init__(self) -> None:
        pass

    # jsonのファイルパス書き換え用関数
    def SetOAuthToken(self, path):
        self.OAuthToken = path
    def SetUpdateToken(self, paht):
        self.UpdateToken = paht

    # API使用のためのトークンの確認&接続
    def ConnectAndGetToken(self):
        # The file token.json stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        # 更新トークンあるかチェック&認証　
        if os.path.exists(self.UpdateToken): #ある時-->認証
            self.Creds = Credentials.from_authorized_user_file(self.UpdateToken, self.SCOPES)

        # If there are no (valid) credentials available, let the user log in.:
        # ないとき or　有効期限切れ
        if not self.Creds or not self.Creds.valid:

            if self.Creds and self.Creds.expired and self.Creds.refresh_token:
                self.Creds.refresh(Request())

            else:
                flow = InstalledAppFlow.from_client_secrets_file(self.OAuthToken, self.SCOPES)
                self.Creds = flow.run_local_server(port=0)

            # Save the credentials for the next run: 次回Runに向けて更新トークンを保存
            with open(self.UpdateToken, 'w') as token:
                token.write(self.Creds.to_json())

    def RunAPI(self):
        try:
            # Call the Calendar API
            # クエリ指定で特定のフォルダ(ラベル)や日時などを指定できると思われ
            # クエリの指定一覧: https://support.google.com/mail/answer/7190?hl=ja
            service = build('calendar', 'v3', credentials=self.Creds)
            NowTimeStump = datetime.datetime.utcnow().isoformat() + 'Z'  # 'Z' indicates UTC time

            print('Getting the upcoming 10 events')
            events_result = service.events().list(calendarId='primary', timeMin=NowTimeStump,
                                            maxResults=10, singleEvents=True,
                                            orderBy='startTime').execute()
            events = events_result.get('items', [])

            if not events:
                print('No upcoming events found.')
                return

            # Prints the start and name of the next 10 events
            for event in events:
                start = event['start'].get('dateTime', event['start'].get('date'))
                print(start, event['summary'])

        except HttpError as error:
            # TODO(developer) - Handle errors from gmail API.
            print(f'An error occurred: {error}')

a =  CalendarCont()
a.ConnectAndGetToken()
a.RunAPI()
```

これを実行すると予定一覧(近日10個)が表示される。実行したアカウントではcalendarを使っていなかったため、

```
No upcoming events found.
```

と表示されたが、試しに「てすと」という名で翌日の3:30~4:30で予定を登録してから実行すると

```
2023-03-31T03:30:00+09:00 てすと
```

と表示された。どうやら終了時間は表示されないらしい(サンプルプログラムとは別のAPIを使用しなければならないのだろう)。形式的に

日付T予定開始時間(+/-UTCからの時差) 予定名

となっているようだ。単純に日付と開始時刻だけでなくUTCとの時差も表記されているようだ。確かにサンプルプログラムにもドキュメントにもUTCを基準とすると書いてある。

ではあとは実際に予定を追加する関数を作ろう。またテストをした段階で研究関係の予定を登録するための項目を`研究`の題でマイカレンダーに追加しておいた。また`RunAPI()`関数はこの後`GetEvents()`関数に名前を変更している。(デバック用に残しておく)

あとは[公式ドキュメント]の「Calendar APIを使用する」-->「イベントの作成」を見ると`events.insert()`を使えばよいらしい。書式については以下の通り

```py
event = service.events().insert(calendarId='primary', body=event).execute()
```

- `calendarId`: カレンダー(=User)ID。メインのcalendarなら`primary`、それ以外ならそのIDを指定。
  - IDが知りたい: カレンダーを選択-->設定-->カレンダー設定へ
  - **項目:カレンダーの統合の真下にcalendarIdが書いてある(ｸｿ長い)**
  - [参考](https://blog-and-destroy.com/41932)）
- `body`: イベント(予定)の内容PythonはJson形式(辞書型)で記入するっぽい。

bodyの形式はこちら
```py
event = {
    'summary': 'イベントのタイトル',
    'location': 'イベントする場所(記入任意)',
    'description': 'イベントの説明',
    'start': # イベント開始
    {
        'dateTime': '2015-05-28T09:00:00-07:00', #時間
        'timeZone': 'America/Los_Angeles',　# タイムゾーン
    },
    'end': # イベント開始
    {
        'dateTime': '2015-05-28T17:00:00-07:00', #時間
        'timeZone': 'America/Los_Angeles',　# タイムゾーン
    },
    'recurrence': # 定期的な予定(1件だけなら省略)
    [
        'RRULE:FREQ=DAILY;COUNT=2' # DAILY: 一日(周期); カウント=2回
    ],
    'attendees': # イベント参加者
    [
        {'email': 'lpage@example.com'},
        {'email': 'sbrin@example.com'},
    ],
    'reminders': # リマインダー設定
    {
        'useDefault': False, # カレンダーのデフォルトのリマインダーが予定に適用されるかどうか。
        'overrides':
        [
            # 例では1日前と10分前に通知している
            {'method': 'email', 'minutes': 24 * 60}, # email送信: 24*60 min=1日前
            {'method': 'popup', 'minutes': 10}, # popupする: 10分前
        ],
    },
}
```

ほかにもいろんな設定ができるっぽい。まぁサンプル前半と通知以外は使う機会がないと思うが。
これをもとに設定ファイル`SetEvent`関数及び、設定ファイルの設定をいじれる関数群を実装する。予定時間の指定はしてはdatetimeモジュールを使って楽をする。

## いざ改造

扱い方がわかってきたところで改造を行う。この改造に非常に時間がかかってしまい、サンプルコードの跡形が殆どない形にまでソースコードが変わってしまった。(時間にして6時間程度費やしてしまった)

### 変更(改造)点

変更した点については以下のとおりである

- スコープの変更: `SCOPES = ['https://www.googleapis.com/auth/calendar']`とする。サンプルコードは読み取り専用になっていて予定の追加ができなくないため。下はそのエラー

```json
{
  "error": {
    "code": 403,
    "message": "The request is missing a valid API key.",
    "errors": [
      {
        "message": "The request is missing a valid API key.",
        "domain": "global",
        "reason": "forbidden"
      }
    ],
    "status": "PERMISSION_DENIED"
  }
}
```

- `RunAPI`をAPIの呼び出し処理をする関数として残した。呼び出し処理をすでにしているかもフラグでチェックしている。これで各関数で呼び直しの処理をしなくても良くなった。
- カレンダーIDを指定するための関数を実装。これに合わせてカレンダーIDもメンバ変数として実装。デフォルト値は`primary`。カレンダーIDは各カレンダーの「設定と共有」-->「カレンダーの統合」から確認できる。
- `def GetCalendarList()`登録しているカレンダーのリストを表示している(本当に`研究`カレンダーが認識されているかを確認するために実装した)
- `def SetEvent()`:本命。指定したカレンダー(カレンダーID)に予定を書き込む。変数は辞書型`ThisEvent`であるが、根本のフォーマットは`ThisEventFormat`辞書により決められる。(後述)

### イベントのフォーマットについて

予定表のフォーマットは以下のようになっている

```python
# -- 予定表のフォーマット
ThisEventFormat = { # 初期化用
    'summary': '',
    # 'location': '',
    # 'description': '',
    'start': # イベント開始
    {
        'dateTime': '+09:00', #時間
        'timeZone': 'Asia/Tokyo', # タイムゾーン(固定)
    },
    'end': # イベント開始
    {
        'dateTime': '+09:00', #時間
        'timeZone': 'Asia/Tokyo', # タイムゾーン(固定)
    },
    # 'reminders': # リマインダー設定
    # {
    #     'useDefault': False, # カレンダーのデフォルトのリマインダーが予定に適用されるかどうか。
    #     'overrides': [] # 通知設定
    # },
    # 'source': # 追加URL
    # {
    #     'url':'',
    #     'title':''
    # }
}
```

必ず必要なのはコメントアウトしていない`summary`、`start`、`end`である。変数の説明もややこしいので表にまとめた

|変数(辞書のタグ)=初期値                       |説明                                                  |設定関数                                  |
|-------------------------------------------|-----------------------------------------------------|-----------------------------------------|
|`ThisEvent['summary']=''`                  | 予定(必ず設定)                                        | `SetEvents(予定,開始時間,終了時間(省略可能))`|
|`ThisEvent[start']['dateTime']='+09:00'`   | 予定開始時間(タイムゾーン固定)                           | `SetEvents(予定,開始時間,終了時間(省略可能))`|
|`ThisEvent[end']['dateTime']='+09:00'`     | 予定終了時間(タイムゾーン固定、無指定で開始から一時間後に設定) | `SetEvents(予定,開始時間,終了時間(省略可能))`|
|`ThisEvent['location'] = None`             | 場所                                                 | `SetLocation(場所)`|
|`ThisEvent['description'] = None`          | 予定の詳細なメモ                                       | `SetMemo(メモ)`|
|`ThisEvent[reminders']['overrides'] = None`| リマインダー設定(method: 通知手法(mail/popup)、 minutes: 通知時間(分) datetimeで指定) | `SetReminder(通知手法, 時間: datatime型)`|
|`ThisEvent['source']['url/title'] = None`  | URL(url、urlのタイトル)を指定                                                     | `SetURL(URL, タイトル)`|


ということでソースコードは以下の通り

```python
#%%
from __future__ import print_function

import os.path
import datetime

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
# pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib
# https://developers.google.com/calendar/api/quickstart/python?hl=ja


class CalendarCont:

    # If modifying these scopes, delete the file token.json.
    # ReadOnlyとる!
    SCOPES = ['https://www.googleapis.com/auth/calendar']
    Creds = None

    OAuthToken = './json/credentials.json' # 認証トークンのPath
    UpdateToken = './json/token_calendar.json' # 更新トークンのpath

    Service = None # API
    fService = True # 起動フラグ

    CalendarID = 'primary'

    # -- 予定表のフォーマット
    ThisEventFormat = { # 初期化用
        'summary': '',
        # 'location': '',
        # 'description': '',
        'start': # イベント開始
        {
            'dateTime': '+09:00', #時間
            'timeZone': 'Asia/Tokyo', # タイムゾーン(固定)
        },
        'end': # イベント開始
        {
            'dateTime': '+09:00', #時間
            'timeZone': 'Asia/Tokyo', # タイムゾーン(固定)
        },
        # 'reminders': # リマインダー設定
        # {
        #     'useDefault': False, # カレンダーのデフォルトのリマインダーが予定に適用されるかどうか。
        #     'overrides': [] # 通知設定
        # },
        # 'source': # 追加URL
        # {
        #     'url':'',
        #     'title':''
        # }
    }
    ThisEvent = ThisEventFormat # 実際はThisEventsのほうをいじる

    ReminderList = []

    # -- ここから関数
    # コンストラクタ
    def __init__(self) -> None:
        pass

    # jsonのファイルパス書き換え用関数
    def SetOAuthToken(self, path):
        self.OAuthToken = path
    def SetUpdateToken(self, path):
        self.UpdateToken = path

    # calendarIdを設定する関数
    def SetCalendarID(self, calendar_id='primary'):
        self.CalendarID = calendar_id

    # API使用のためのトークンの確認&接続
    def ConnectAndGetToken(self):
        # The file token.json stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        # 更新トークンあるかチェック&認証　
        if os.path.exists(self.UpdateToken): #ある時-->認証
            self.Creds = Credentials.from_authorized_user_file(self.UpdateToken, self.SCOPES)

        # If there are no (valid) credentials available, let the user log in.:
        # ないとき or　有効期限切れ
        if not self.Creds or not self.Creds.valid:

            if self.Creds and self.Creds.expired and self.Creds.refresh_token:
                self.Creds.refresh(Request())

            else:
                flow = InstalledAppFlow.from_client_secrets_file(self.OAuthToken, self.SCOPES)
                self.Creds = flow.run_local_server(port=0)

            # Save the credentials for the next run: 次回Runに向けて更新トークンを保存
            with open(self.UpdateToken, 'w') as token:
                token.write(self.Creds.to_json())

    # API起動のためだけに復活
    def RunAPI(self):
        # Call the Calendar API
        if self.fService:
            self.Service = build('calendar', 'v3', credentials=self.Creds)
            self.fService = False

    # 登録しているカレンダーのリストを表示
    def GetCalendarList(self):
        self.RunAPI()

        while True:
            CalendarList = self.Service.calendarList().list(pageToken=None).execute()
            for calendar_list_entry in CalendarList['items']:
                print(calendar_list_entry['summary'])
                # print(calendar_list_entry)
            page_token = CalendarList.get('nextPageToken')
            if not page_token:
                break

    def GetEvents(self):
        try:
            # Call the Calendar API
            self.RunAPI()

            NowTimeStump = datetime.datetime.utcnow().isoformat() + 'Z'  # 'Z' indicates UTC time

            MaxResult = 10
            print('Getting the upcoming {} events'.format(MaxResult))
            events_result = self.Service.events().list(
                calendarId=self.CalendarID,
                timeMin=NowTimeStump,
                maxResults=MaxResult,
                singleEvents=True,
                orderBy='startTime'
                ).execute()

            events = events_result.get('items', [])

            if not events:
                print('No upcoming events found.')
                return

            # Prints the start and name of the next 10 events
            for event in events:
                start = event['start'].get('dateTime', event['start'].get('date'))
                print(start, event['summary'])

        except HttpError as error:
            # TODO(developer) - Handle errors from gmail API.
            print(f'An error occurred: {error}')

    def SetEvent(self):
        self.RunAPI()
        # print(self.CalendarID)
        # print(self.ThisEvent)

        try:
            CalendarList = self.Service.calendarList().get(calendarId=self.CalendarID).execute()
            # print(CalendarList)
            Event = self.Service.events().insert(
                calendarId=self.CalendarID, body=self.ThisEvent).execute()

            print(Event['id'])
            print(Event)

            self.EventClear()

            return Event

        except Exception as e:
            print(e)

    # 以下予定登録関係の設定をする関数
    # 最低限の予定入力
    def SetEvents(self, summary='', start_time=None, end_time=None):
        # タイトル
        self.ThisEvent['summary'] = summary

        if start_time is not None:
            self.ThisEvent['start']['dateTime'] = start_time.isoformat()

        if end_time is None and start_time is not None: # 終了時間ないとき
            end_time = +datetime.timedelta(hours=1) # 一時間のみを作る
            end_time += start_time # これで開始時間から1時間だけ追加した時間を終了時間とする

        self.ThisEvent['end']['dateTime'] = end_time.isoformat()

        # print(self.ThisEvent['summary'])
        # print(self.ThisEvent['start']['dateTime'])
        # print(self.ThisEvent['end']['dateTime'])

    # location
    def SetLocation(self, location=''):
        self.ThisEvent['location'] = location

    # memo
    def SetMemo(self, memo=''):
        self.ThisEvent['description'] = memo

    # リマインダの設定
    def SetReminder(self, method=None, rem_time=None):
        RemindDict = {'method': 'email', 'minutes': None}

        if method is not None:
            RemindDict['method'] = method

        if rem_time is None:
            rem_time = datetime.timedelta(minutes=10)

        # print(int(rem_time.total_seconds()/60))
        RemindDict['minutes'] = int(rem_time.total_seconds()/60)

        self.ReminderList.append(RemindDict)

        self.ThisEvent['overrides'] = self.ReminderList
        # print(self.ThisEvent['overrides'] )
        pass

    # URLの設定
    def SetURL(self, url=None, title=None):
        if url is not None:
            self.ThisEvent['source']['url'] = url
        if title is not None:
            self.ThisEvent['source']['title'] = title

    # 初期化
    def EventClear(self):
        self.ThisEvent = self.ThisEventFormat

time = datetime.datetime(year=2023, month=3, day=30, hour=12, minute=12)
a = time.strftime('%Y-%m-%dT%H:%M:%S+09:00')
a =  CalendarCont()
a.ConnectAndGetToken()
a.SetCalendarID(calendar_id='[カレンダーID]')
# a.SetCalendarID()
# a.GetCalendarList()
a.SetEvents('aaaa', time)
# a.SetReminder(rem_time=60)
# a.SetReminder(rem_time=120)
# a.SetReminder(rem_time=datetime.timedelta(days=1))
a.SetEvent()
# a.GetEvents()
# %%
```

試しに3/30の12:42にaaaaの予定を追加してテストしたものがこちら。

![calendar_test](../image/memo/calendar_test.png){width="800"}

###### 実行結果(`SetEvent()`の`Events`の結果が表示されている)

```
{'kind': 'calendar#event',
 'etag': '"***********"',
 'id': '**********',
 'status': 'confirmed',
 'htmlLink': '**追加したイベントへのリンク**',
 'created': '2023-03-30T07:58:23.000Z', <-- UTC基準っぽい
 'updated': '2023-03-30T07:58:23.847Z', <-- UTC基準っぽい
 'summary': 'aaaa', <--ここから追加したイベントの詳細
 'creator': {'email': '***********@gmail.com'},
 'organizer': {'email': 'カレンダーID',
  'displayName': '研究',
  'self': True},
 'start': {'dateTime': '2023-03-30T12:12:00+09:00', 'timeZone': 'Asia/Tokyo'},
 'end': {'dateTime': '2023-03-30T13:12:00+09:00', 'timeZone': 'Asia/Tokyo'},
 'iCalUID': '***************',
 'sequence': 0,
 'reminders': {'useDefault': True},
 'eventType': 'default'}
```

これで予定追加もできるようになった。やったぜ。

[公式ドキュメント]: https://developers.google.com/calendar/api/quickstart/python?hl=ja